//
//  Classes.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 23/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import UIKit

class AllCornerButton: UIButton {
    @IBInspectable var radius: CGFloat = 10
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: radius)
    }
}

class RedButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let cornerRadius: CGFloat = 20.0
        self.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: cornerRadius)
        self.titleEdgeInsets.left = 20
        self.titleEdgeInsets.right = 20
        self.setTitleColor(UIColor.white, for: .normal)
        self.setBackgroundImage(Utils.image(withColor: UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)), for: .normal)
        self.setBackgroundImage(Utils.image(withColor: UIColor(hexString: "#cccccc")!), for: .disabled)
        
    }
}

class GrayButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let cornerRadius: CGFloat = 20.0
        self.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: cornerRadius)
        self.titleEdgeInsets.left = 20
        self.titleEdgeInsets.right = 20
        self.setTitleColor(UIColor.white, for: .normal)
        self.backgroundColor = UIColor(hexString: "#cccccc")
    }
}

class AllCornerTextField: UITextField {
    @IBInspectable var radius: CGFloat = 10
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: radius)
    }
}

class AllCornerView: UIView {
    @IBInspectable var radius: CGFloat = 10
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: radius)
//        self.layer.masksToBounds = true
    }
    
//    func layoutCornerView() {
//        self.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: radius)
//    }
}
