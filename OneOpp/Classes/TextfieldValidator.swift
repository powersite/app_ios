//
//  TextfieldValidator.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 12/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import UIKit

class TextfieldValidator {
    static let EMAIL_ADDRESS =
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+"

    // Validator e-mail from string
    static func isValidEmail(_ value: String) -> Bool {
        let string = value.trimmingCharacters(in: .whitespacesAndNewlines)
        let predicate = NSPredicate(format: "SELF MATCHES %@", TextfieldValidator.EMAIL_ADDRESS)
        return predicate.evaluate(with: string) || string.isEmpty
    }
    
    static func isValidEmail(field: UITextField, show: Bool = true) -> Bool {
        if TextfieldValidator.isValidEmail(field.text!) {
            field.setError()
            return true
        } else {
            field.setError("Email inválido", show: show)
        }
        return false
    }
    
    static func checkMinLength(_ field: UITextField, _ minLenght: Int, errorMsg: String, futureText: String = "") -> Bool {
        let text = field.text!
        
        var string: String
        if futureText.isEmpty {
            string = text.trimmingCharacters(in: .whitespacesAndNewlines)
        } else {
            string = futureText
        }
        
        let res = (string.count >= minLenght)
        if res {
            field.setError()
        } else {
            field.setError(errorMsg, show: true)
        }
        return res
    }
    
    static func checkEmpty(_ field: UITextField, futureText: String = "") -> Bool {
        let text = field.text!
        
        var string: String
        if futureText.isEmpty {
            string = text.trimmingCharacters(in: .whitespacesAndNewlines)
        } else {
            string = futureText
        }
        
        let isEmpty = string.isEmpty
        if isEmpty {
            field.setError("El campo es requerido", show: true)
        } else {
            field.setError()
        }
        return !isEmpty
    }
    
    static func checkEmptyFields( _ textfields: [UITextField]) -> Bool {
        var res = true
        for field in textfields {
            let text = field.text!
            let string = text.trimmingCharacters(in: .whitespacesAndNewlines)
            if string.isEmpty {
                field.setError("El campo es requerido", show: true)
                res = false
            }
        }
        return res
    }
    
    static func checkEqualPass(tf1: UITextField, tf2: UITextField) -> Bool {
        if tf1.text == tf2.text {
            return true
        } else {
            tf2.setError("Las contraseñas no coinciden.", show: true)
            return false
        }
    }
    
    // El nombre debe contener al menos 3 caracteres
    // Email inválido
    // El password es muy corto (>3)
    // Las contraseñas no coinciden.
}
