//
//  Skinning.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 23/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import UIKit
import RappleProgressHUD

struct Skinning {
    static let attributesForProgressHud = [RappleTintColorKey : UIColor.green]
    
    static func skinFormButton(button: UIButton) {
        let cornerRadius: CGFloat = 10.0
        button.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: cornerRadius)

        button.setTitleColor(UIColor.white, for: .normal)
//        button.titleLabel?.font = UIFont(name: "Orgon-Medium", size: 12.0)
        button.backgroundColor = UIColor(hexString: "#ec2c49")
    }
    
    static func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {

        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x: 0, y: size.height-lineSize.height, width: lineSize.width, height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    static func styleHowToTabBar() {
        let w = UIApplication.shared.windows.first
        
        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.selectionIndicatorImage = Skinning.getImageWithColorPosition(color: .black, size: CGSize(width:(w?.frame.size.width)!/4,height: 49), lineSize: CGSize(width:(w?.frame.size.width)!/4, height:2))
        tabBarAppearance.tintColor = .orange
        
        let tabBarItemAppearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14)!]
        tabBarItemAppearance.setTitleTextAttributes(attributes, for: .normal)
        tabBarItemAppearance.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -15)
    }
    
    static func styleMainTabBar() {
//        let w = UIApplication.shared.windows.first
        
        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.selectionIndicatorImage = nil
        tabBarAppearance.tintColor = .black
        
        let tabBarItemAppearance = UITabBarItem.appearance()
//        let attributes = [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 14)!]
        tabBarItemAppearance.setTitleTextAttributes(nil, for: .normal)
        tabBarItemAppearance.titlePositionAdjustment = UIOffset.zero
    }
}
