//
//  SortYellowButton.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 27/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class SortYellowButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = self.frame.size.width/2
        layer.borderColor = UIColor(red: 232/255, green: 76/255, blue: 141/255, alpha: 1).cgColor
        clipsToBounds = true
        self.setBackgroundImage(Utils.image(withColor: UIColor.clear), for: .normal)
        self.setBackgroundImage(Utils.image(withColor: UIColor(red: 243/255, green: 156/255, blue: 70/255, alpha: 1.0)), for: .selected)
        self.setBackgroundImage(Utils.image(withColor: UIColor(red: 243/255, green: 156/255, blue: 70/255, alpha: 1.0)), for: .highlighted)

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if self.isSelected {
            layer.borderWidth = 0
        } else {
            layer.borderWidth = 1
        }
    }
    
    override var isSelected: Bool {
        didSet {
            if self.isSelected {
                layer.borderWidth = 0
            } else {
                layer.borderWidth = 1
            }
        }
    }
}
