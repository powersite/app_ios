//
//  Utilis.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import UIKit
import RappleProgressHUD
import BRYXBanner

struct Utils {
    struct Constants {
        static let COMMERCE_URL = " https://testfe.one-opp.com"//"http://oneopp-fe.hopto.org"
        static let DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=com.quacero.oneopp" // TODO: ver como manejar el tema de las urls de las plataformas
        static let BENEFIT_SHARE = "OneOpp - @business \n@discount @description\n\n\(Constants.DOWNLOAD_URL)"
    }
    static func readJSONFromFile(fileName: String) -> Any? {
        var json: Any?
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                // Getting data from JSON file using the file URL
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                json = try? JSONSerialization.jsonObject(with: data)
            } catch {
                // Handle error here
            }
        }
        return json
    }
    
    static func startProgressAnimation() {
        RappleActivityIndicatorView.startAnimating()
//        RappleActivityIndicatorView.startAnimating(attributes: Skinning.attributesForProgressHud)
    }
    
    static func stopProgressAnimation() {
        RappleActivityIndicatorView.stopAnimation()
    }
    
    static func shareBenefit(textToShare: String, vc: UIViewController) {
       let activityViewController = UIActivityViewController(activityItems: [textToShare], applicationActivities: nil)
       activityViewController.popoverPresentationController?.sourceView = vc.view

       activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop ]

       vc.present(activityViewController, animated: true, completion: nil)
   }
    
    static func image(withColor color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        
        let clipPath = UIBezierPath(rect: rect).cgPath//UIBezierPath(roundedRect: rect, cornerRadius: 5.0).cgPath
        
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.addPath(clipPath)
        context?.setFillColor(color.cgColor)
        context?.fillPath()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    static func showBannerSuccess(message: String?) {
        if let color = UIColor.init(hexString: "#323232") {
            let banner = Banner(title: "", subtitle: message, backgroundColor: color)
            banner.position = .bottom
            banner.dismissesOnTap = true
            banner.show(duration: 5.0)
        }
    }
     
//    static func hexStringToUIColor (hex:String) -> UIColor {
//        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
//
//        if (cString.hasPrefix("#")) {
//            cString.remove(at: cString.startIndex)
//        }
//
//        if ((cString.count) != 6) {
//            return UIColor.gray
//        }
//
//        var rgbValue:UInt32 = 0
//        Scanner(string: cString).scanUInt64(&rgbValue)// .scanHexInt32(&rgbValue)
//
//        return UIColor(
//            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
//            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
//            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
//            alpha: CGFloat(1.0)
//        )
//    }
}
