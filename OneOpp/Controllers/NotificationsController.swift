//
//  NotificationsController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 04/09/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo

protocol NotificationsControllerDelegate: class {
    func onNewNotifications()
}

class NotificationsController {
    static let shared = NotificationsController()
    var delegate: NotificationsControllerDelegate?
    
    
    let fileManager: FileManager
    static let JSON_FILE_NAME = "one-opp_notifications"
    
    init(fileManager: FileManager = .default) {
        self.fileManager = fileManager
        self.readFromJSONFile()
        
        guard let _ = self.notifications else {
            self.notifications = [NotificationModel]()
            return
        }
    }

    var notifications: [NotificationModel]?
    
    func readFromJSONFile() {
        guard let url = makeURL(forFileNamed: NotificationsController.JSON_FILE_NAME) else {
//            throw Error.invalidDirectory
            print("invalidDirectory")
            return
        }
        guard fileManager.fileExists(atPath: url.path) else {
//            throw Error.fileNotExists
            print("fileNotExists")
            
            //TODO: remove
//            if let jsonItems = Utils.readJSONFromFile(fileName: "TestNotifications"), let items = jsonItems as? Dictionary<String, Any>{
//                if let notificationsJS = items["notifications"] {
//                    let notifs:[NotificationModel]? = decode(notificationsJS)
//                    if let n = notifs {
//                        self.notifications = n///*.filter { (notif) -> Bool in
//                    }
//                }
//            }
            
            return
        }
        
        do {
            let data = try Data(contentsOf: url, options: .mappedIfSafe)
            if let json = try? JSONSerialization.jsonObject(with: data) {
                if let dic = json as? Dictionary<String, Any> {
                    if let notificationsJS = dic["notifications"] {
                        let notifs:[NotificationModel]? = decode(notificationsJS)
                        self.notifications = notifs
//                        return notifs
                    }
                }
            }
            //            return nil
        } catch {
            debugPrint(error)
            print("readingFailed")
        }
    }
    
    func addNotification(_ notification: NotificationModel) {
        if let alreadyExist = self.notifications?.contains(where: { notif in
            return notification.id == notif.id
        }), alreadyExist == true {
            return
        }
        
        self.notifications?.insert(notification, at: 0)
        self.saveToJSONFile()
    }
    
    private func saveToJSONFile() {
        let obj = ["notifications": notifications]
        guard let url = makeURL(forFileNamed: NotificationsController.JSON_FILE_NAME) else {
            print("invalidDirectory")
            return
        }
        do {
            let data = try JSONEncoder().encode(obj)
            try data.write(to: url)
            self.delegate?.onNewNotifications()
        } catch {
            debugPrint(error)
            print("writtingFailed")
        }
    }
    
    private func makeURL(forFileNamed fileName: String) -> URL? {
        guard let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        return url.appendingPathComponent(fileName)
    }
}
