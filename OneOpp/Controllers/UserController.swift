//
//  UserController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 25/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo
import SwiftKeychainWrapper
import Firebase
import FirebaseAuth

class UserController {
    struct Constants {
        static let TOKEN = "OO_token"
        static let OO_NOTIF_BENEFIT = "notification_benefit"
        static let DELIVERY_INFO_PREFIX = "delivery_info_"
    }
    
    enum TokenType: String {
        case regular
        case google
    }
    
//    lazy var notificationAllowed: Bool? {
//
//    }
    private(set) lazy var notificationAllowed = getNotifPermission()
    
    private func getNotifPermission() -> Bool? {
        return UserDefaults.standard.bool(forKey: Constants.OO_NOTIF_BENEFIT + "_" + (self.user?.email ?? ""))
    }
    
    static let shared = UserController()
    
    public var user: User?
    
    private func saveToken(accessToken: String, type: TokenType) {
        self._token = accessToken
        // Guarda el token y si es login comun o de Google
        // TODO: Evaluar el manejo como dict
////        UserDefaults.standard.set(type.rawValue, forKey: Constants.TOKENTYPE)
//        UserDefaults.standard.synchronize()
        _ = KeychainWrapper.standard.set(self.token!, forKey: Constants.TOKEN)
    }
    
    public func changeNotificationPermission(to notifConfig: Bool) {
        notificationAllowed = notifConfig
        UserDefaults.standard.set(notifConfig, forKey: Constants.OO_NOTIF_BENEFIT + "_" + (self.user?.email ?? ""))
    }
    
    public func getNotificationPermission() -> Bool {
        return notificationAllowed ?? false
    }
    
//    func setNotificationAllowed(_ notif: Bool) {
//        UserDefaults.standard.set(notif, forKey: Constants.OO_NOTIF_BENEFIT)
//    }
    
    var _email: String?
    
    var _password: String?
    
    var _token: String?
    var token: String? {
        if _token == nil {
            _token = KeychainWrapper.standard.string(forKey: Constants.TOKEN)
        }
        return _token
        
    }
        
    func closeSession() {
        do {
            self._token = nil
            self._email = nil
            self._password = nil
//            self._tokenType = nil
            
            // TOOO: cambiar por eso:
            // NavigationController.shared.showLogInScreen()
//            UserDefaults.standard.removeObject(forKey: Constants.TOKENTYPE)
//            UserDefaults.standard.removeObject(forKey: Constants.EMAIL)
            KeychainWrapper.standard.removeObject(forKey: Constants.TOKEN)
//            KeychainWrapper.standard.removeObject(forKey: Constants.PASS)
            deleteDeliveryInfo()
            try Auth.auth().signOut()
        } catch let error {
            print(error.localizedDescription)
        }
    }
        
    func register(email: String,name: String, password: String, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        RegisterService.register(email: email, name: name, password: password, completionSuccess: {
            completionSuccess()
        }) { (error) in
            completionError(error)
        }
        
    }
    
    func checkPreviousLogin(completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        if let currentUser = Auth.auth().currentUser {
            currentUser.getIDTokenResult(forcingRefresh: true) { (authTokenResult, error) in
                if let _ = error {
                    completionError(nil)
                    return
                }
                guard let token = authTokenResult?.token else {
                    completionError(nil)
                    return
                }
                print(token)
                self.logInSocial(token: token, completionSuccess: {
                    let user = User(name: currentUser.displayName, email: currentUser.email, activated: true, userImage: currentUser.photoURL?.absoluteString)
                    self.user = user
//                    NavigationController.shared.showMainLoggedInNavigationScreen()
                    completionSuccess()
                }) { (apiError) in
                    print(apiError?.message ?? "error")
                    completionError(apiError)
                }
            }
        } else {
            self.userProfile(completionSuccess: {
                completionSuccess()
            }) { (error) in
                print("user profile error")
                completionError(error)
            }
        }
        
    }
    
    func logIn(email: String, password: String, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {

        LoginService.regularLogin(email: email, password: password, completionSuccess: { (token) in
            
            self.saveToken(accessToken: token, type: .regular)
            completionSuccess()

            self.userProfile(completionSuccess: {
            }) { (error) in
                print("user profile error")
                completionError(error)
            }
        }) { (error) in
            print(error?.message ?? "")
            completionError(error)
        }
    }
    
    func firLogin(credential: AuthCredential, completionSuccess: @escaping() -> Void, completionError: @escaping () -> Void) {
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let _ = error { return }
            authResult?.user.getIDTokenResult(forcingRefresh: true, completion: { (authTokenResult, error) in
                if let _ = error { return }
                if let token = authTokenResult?.token  {
                    print("token: \(token)") // este anduvo
                    UserController.shared.logInSocial(token: token, completionSuccess: {
                        
                        if let gUser = authResult?.user {
                            let user = User(name: gUser.displayName, email: gUser.email, activated: true, userImage: gUser.photoURL?.absoluteString)
                            UserController.shared.user = user
                        }
                        /*Utils.stopProgressAnimation()
                        self.performSegue(withIdentifier: "showMainScreen", sender: nil)*/
                        completionSuccess()
                    }) { (_) in
                        print("error")
//                        Utils.stopProgressAnimation()
                        completionError()
                    }
                } else {
//                    Utils.stopProgressAnimation()
                    completionError()
                }
            })
          // User is signed in
          // ...
        }
    }
    
    private func logInSocial(token: String, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        LoginService.socialLogin(token: token, completionSuccess: { (accessToken) in
            let accessToken = "Bearer " + accessToken
            self.saveToken(accessToken: accessToken, type: .regular)
            
            UserController.fcm{
                print("fcm updated successfully")
            } completionError: { (error) in
                print("fcm not updated")
            }
            
            completionSuccess()
        }) { (error) in
            print(error?.message ?? "")
            completionError(error)
        }
    }
    
    func userProfile(completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        let userProfileServiceRequest = UserProfileServiceRequest()
        
        NetworkingController.get(request: userProfileServiceRequest, completionSucessfully: { (json) in
            if let json = json {
                self.user = decode(json)
            }
            UserController.fcm{
                print("fcm updated successfully")
            } completionError: { (error) in
                print("fcm not updated")
            }
            completionSuccess()
        }) { (error) in
            print(error.message ?? "")
            completionError(error)
        }
    }
    
    func sendConfigurationForUser(allow: Bool, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        ConfigurationService.sendConfiguration(allowNotif: allow, completionSuccess: {
            self.changeNotificationPermission(to: allow)
            completionSuccess()
        }, completionError: completionError)
    }
    
    // Delivery
    func getSavedDeliveryInfo() -> Delivery? {
        do {
            guard let email = user?.email, let deliveryInfo = try UserDefaults.standard.getObject(forKey: UserController.Constants.DELIVERY_INFO_PREFIX + email, castTo: Delivery?.self) else {
                return nil
            }
            return deliveryInfo
        } catch {
            print("error")
            return nil
        }
    }
    
    func saveDeliveryInfo (delivery: Delivery) {
        let userDefaults = UserDefaults.standard
        do {
            if let email = user?.email {
                try userDefaults.setObject(delivery, forKey: UserController.Constants.DELIVERY_INFO_PREFIX + email)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteDeliveryInfo () {
        if let email = user?.email {
            UserDefaults.standard.removeObject(forKey: UserController.Constants.DELIVERY_INFO_PREFIX + email)
        }
    }
    
    static func fcm(completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        guard let fcmToken = UserDefaults.standard.string(forKey: "FCMToken") else {
            let error = ApiError(status: nil, error: "Error", message: "")
            completionError(error)
            return
        }
        let loginServiceReq = FCMServiceRequest()
        loginServiceReq.fcmToken = fcmToken
    
        NetworkingController.post(request: loginServiceReq, completionSuccess: completionSuccess, completionError: completionError)
    }
}
