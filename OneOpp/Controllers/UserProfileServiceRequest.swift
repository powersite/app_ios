//
//  UserProfileServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class UserProfileServiceRequest: PostServiceRequest {
    var name: String?
    var email: String?
    var pass: String?
    
    init()
    {
        super.init(serviceName: "account/profile")
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["name"] = self.name
        params["email"] = self.email
        params["password"] = self.pass
        
        return params
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
}
