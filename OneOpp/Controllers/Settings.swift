//
//  Settings.swift
//  ReconDemo
//
//  Created by Vanina Geneiro on 24/03/20.
//  Copyright © 2020 Vanina Geneiro . All rights reserved.
//

import UIKit

class Settings: NSObject
{
    static let shared = Settings()
    
    public var environmentName:String?
//    public var APIKey:String?
    public var APIURL:String?
    public var authorityPath:String?
    public var thirdParty:Dictionary<String,Any>?
    public var program:String?
    public var platform:String?
    public var appName:String?
    public var clientId:String?
    public var clientSecret:String?
    public var tenantId:String?
    public var resourceId:String?
    
    private let prefixEnvironment = "LWServerKeys-"
    private let environmentSelectedKey = "KLWEnvironmentSelected"
    
    private override init() {
        
        super.init()
        
        //self.recoverEnvironmentSelected()
    }
    
    private func saveEnvironmentSelected()
    {
        let userDefaults = UserDefaults.standard
        userDefaults.set(self.environmentName, forKey: environmentSelectedKey)
    }
    
    private func recoverEnvironmentSelected()
    {
        let userDefaults = UserDefaults.standard
        if let environmentRecovered = userDefaults.object(forKey: environmentSelectedKey) as? String
        {
            self.selectEnvironment(environmentSelected: environmentRecovered)
        }
    }
    
    private func plistPathFiles() -> Array<String>
    {
        let resourcePath = Bundle.main.resourcePath
        let files = try! FileManager.default.contentsOfDirectory(atPath: resourcePath!)
        let filtersFiles = files.filter { (fileName:String) -> Bool in
            return fileName.contains(prefixEnvironment)
        }
        var paths = Array<String>()
        for fileName in filtersFiles
        {
            let range = fileName.range(of: ".plist")
            var fileNameWithoutExtentions = fileName
            fileNameWithoutExtentions.removeSubrange(range!)
            let pathFile = Bundle.main.path(forResource: fileNameWithoutExtentions, ofType: "plist")
            paths.append(pathFile!)
        }
        
        return paths
    }
    
    func enviroments() -> Array<String>
    {
        var environmentNames = Array<String>()
        let pathFiles = plistPathFiles()
        for pathFile in pathFiles
        {
            if let settings = NSDictionary(contentsOfFile: pathFile), let environmentName = settings["EnvironmentName"] as? String
            {
                environmentNames.append(environmentName)
            }
            else
            {
                environmentNames.append(pathFile)
            }
        }
        
        return environmentNames
    }
    
    func selectEnvironment(environmentSelected:String)
    {
        let pathFiles = plistPathFiles()
        for pathFile in pathFiles
        {
            if let settings = NSDictionary(contentsOfFile: pathFile), let environmentName = settings["environmentName"] as? String, environmentName == environmentSelected
            {
                self.environmentName = environmentName
//                self.APIKey = settings["APIKey"] as? String
                self.clientId = settings["client_id"] as? String
                self.clientSecret = settings["client_secret"] as? String
                self.tenantId = settings["tenant_id"] as? String
                self.resourceId = settings["resource_id"] as? String
                self.appName = settings["app_name"] as? String
                self.APIURL = settings["site_domain"] as? String
                self.authorityPath = settings["login_authority"] as? String
//                self.thirdParty = settings["ThirdPartyEndpoints"] as? Dictionary<String,Any>
                
                self.saveEnvironmentSelected()
                
                break
            }
        }
        
//        let pathFileGeneralSettings = Bundle.main.path(forResource: "ReconDemoServerKeys", ofType: "plist")
//        if let generalSettings = NSDictionary(contentsOfFile: pathFileGeneralSettings!)
//        {
//            self.program = generalSettings["program"] as? String
//            self.platform = generalSettings["platform"] as? String
//        }
    }
    
}
