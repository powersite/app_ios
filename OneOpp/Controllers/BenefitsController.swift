//
//  BenefitsController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation

protocol BenefitsControllerDelegate: class {
  func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?)
  func onFetchFailed(with reason: String)
}

final class BenefitsController {
  private weak var delegate: BenefitsControllerDelegate?
  
  private var benefits: [Benefit] = []
  private var currentPage = 0
  private var total = 0
  private var isFetchInProgress = false
  private var deliveries: [Delivery]?
  
//  let benefitService = BenefitService()
  let request: GetBenefitServiceRequest
  
  init(request: GetBenefitServiceRequest, delegate: BenefitsControllerDelegate) {
    self.request = request
    self.delegate = delegate
  }
  
  var totalCount: Int {
    return total
  }
  
  var currentCount: Int {
    return benefits.count
  }
  
  func benefit(at index: Int) -> Benefit {
    return benefits[index]
  }
  
    func fetchBenefits(_ term: String? = "") {
    // 1
    guard !isFetchInProgress else {
      return
    }
        
    let serialQueue = DispatchQueue(label: "oneopp.serial.queue")

    serialQueue.async {
        print("Task 1 started")
        // Get Deliveries
        DeliveryService.getDeliveries { (deliveries) in
            self.deliveries = deliveries
            print("Task 1 finished")
        } completionError: { (error) in
            print("error fetching deliveries")
        }
    }
    serialQueue.async {
        print("Task 2 started")
        // 2
        self.isFetchInProgress = true
        self.request.search_term = term
        BenefitService.getBenefits(req: self.request, page: self.currentPage, completionSucessfully: { (pagedBenefitsResponse) in
            // 1
            self.currentPage += 1
            self.isFetchInProgress = false
            // 2
            self.total = pagedBenefitsResponse.total
            let bens = pagedBenefitsResponse.benefits.map { (ben) -> Benefit in
                var benefit = ben
                if let delivery = self.deliveries?.filter({ (delivery) -> Bool in
                    delivery.benefitId == benefit.idBeneficio
                }).first {
                    benefit.deliveryByCoupon = delivery
                }
                return benefit
            }
            self.benefits.append(contentsOf: bens)
            
            // 3
    //        if response.page > 1 {
            if self.currentPage > 1 {
                let indexPathsToReload = self.calculateIndexPathsToReload(from: pagedBenefitsResponse.benefits)
              self.delegate?.onFetchCompleted(with: indexPathsToReload)
            } else {
              self.delegate?.onFetchCompleted(with: .none)
            }
        }) { (error) in
            DispatchQueue.main.async {
              self.isFetchInProgress = false
              self.delegate?.onFetchFailed(with: error)
            }
        }

        print("Task 2 finished")
    }
    }
       
    func searchBenefits(_ term: String) {
        
    }
    
    /*benefitService.fetchModerators(with: request, page: currentPage) { result in
      switch result {
      // 3
      case .failure(let error):
        DispatchQueue.main.async {
          self.isFetchInProgress = false
          self.delegate?.onFetchFailed(with: error.reason)
        }
      // 4
      case .success(let response):
        DispatchQueue.main.async {
          // 1
          self.currentPage += 1
          self.isFetchInProgress = false
          // 2
          self.total = response.total
          self.benefits.append(contentsOf: response.moderators)
          
          // 3
          if response.page > 1 {
            let indexPathsToReload = self.calculateIndexPathsToReload(from: response.moderators)
            self.delegate?.onFetchCompleted(with: indexPathsToReload)
          } else {
            self.delegate?.onFetchCompleted(with: .none)
          }
        }
      }
    }
  }*/
  
  private func calculateIndexPathsToReload(from newBenefits: [Benefit]) -> [IndexPath] {
    let startIndex = benefits.count - newBenefits.count
    let endIndex = startIndex + newBenefits.count
    return (startIndex..<endIndex).map { IndexPath(row: $0, section: 0) }
  }
  
}

