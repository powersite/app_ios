//
//  NetworkingController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Alamofire
import Argo

struct NetworkingController {
    struct Constants {
        static let BASE_URL = "http://18.224.239.224:4000/api"
        static let IMAGES_PATH = "https://testfe.one-opp.com/uploads/logos/" //"http://oneopp-fe.hopto.org/uploads/logos/"
        static let BENEFIT_IMAGE_PATH = "https://testfe.one-opp.com/uploads/benefit/"

    }
    
//    static let shared = NetworkingController()
        
    static func get(request:GetServiceRequest,completionSucessfully: @escaping (Any?) -> (),completionError: @escaping (ApiError) -> ())
    {
        let urlString = request.urlString()

        let headers = request.getHeaders()
        
        let parameters = request.getParameters()
        
        AF.request(urlString, parameters: parameters, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                switch response.result {
                    case .success(let json):
                        print("Success with JSON: \(json)")
                    
                        completionSucessfully(json)

                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        var errorString: String?
                        if let data = response.data {
                            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any] {
                               errorString = json["message"] as? String
                           }
                        }

                        print(String(errorString ?? "")) //Contains General error message or specific.
                        if let _ = errorString {
                            completionError(ApiError(message: errorString))
                        } else {
                            completionError(ApiError(message: error.errorDescription))
                    }

                }
        }
        
    }
    
    static func post(request:PostServiceRequest,completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError) -> ())
    {
        let urlString = request.urlString()
                
        let headers = request.getHeaders()
        
        let parameters = request.getParameters()
        
        let encoding = request.getEncoding()
        
        AF.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.response?.statusCode == 201 || response.response?.statusCode == 200) {
                   completionSuccess()
                } else {
                    if let data = response.data {
                        
                        do{
                            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                                if let error: ApiError = decode(json) {
                                    completionError(error)
                                }
                            }
                        } catch {
                            print("erroMsg")
                            
                            let json = String(data: data, encoding: String.Encoding.utf8)
                            completionError(ApiError(status: nil, error: "Register error", message: json))
                        }
                    }
                    let error = ApiError(status: nil, error: "Error", message: "")
                   completionError(error)
                }
        }
    }
    
    static func loginPost(request:PostServiceRequest,completionSuccess: @escaping (String) -> (),completionError: @escaping (ApiError) -> ())
    {
        let urlString = request.urlString()
        
        let headers = request.getHeaders()
        
        let parameters = request.getParameters()
        
        AF.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.response?.statusCode == 200) {
                    if let headerFields = response.response?.allHeaderFields {
                        if let access_token = headerFields["Authorization"] as? String {
                            completionSuccess(access_token)
                        }
                        print(headerFields)
                    }
                } else {
                    if let data = response.data {
                        
                        do{
                            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                                if let error: ApiError = decode(json) {
                                    completionError(error)
                                }
                            }
                        } catch {
                            print("erroMsg")
                            
                        }
                    }
                    let error = ApiError(status: nil, error: "Error", message: "Login error")
                    completionError(error)
                }
        }
        
        
    }
    
    static func socialLoginPost(request:PostServiceRequest,completionSuccess: @escaping (String) -> (),completionError: @escaping (ApiError) -> ())
    {
        let urlString = request.urlString()
        
        let headers = request.getHeaders()
        
        let parameters = request.getParameters()
        
        AF.request(urlString, method: .post, parameters: parameters, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                switch response.result {
                    case .success(let json):
                        print("Success with JSON: \(json)")
                        if let dic = json as? NSDictionary, let access_token = dic["id_token"] as? String {
                            completionSuccess(access_token)
                        }
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                        completionError(ApiError(message: error.errorDescription))
                        
                }
        }
        
        
    }
    
    static func patch(request:PatchServiceRequest, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError) -> ())
    {
        let urlString = request.urlString()
                
        let headers = request.getHeaders()
        
        let parameters = request.getParameters()
        
        let encoding = request.getEncoding()
        
//        let httpBody = request.getBodyParameters()
        AF.request(urlString, method: .patch, parameters: parameters, encoding: encoding, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.response?.statusCode == 201 || response.response?.statusCode == 200) {
                   completionSuccess()
                } else {
                    if let data = response.data {
                        
                        do{
                            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                                if let error: ApiError = decode(json) {
                                    completionError(error)
                                }
                            }
                        } catch {
                            print("erroMsg")
                            
                            let json = String(data: data, encoding: String.Encoding.utf8)
                            completionError(ApiError(status: nil, error: "Register error", message: json))
                        }
                    }
                    let error = ApiError(status: nil, error: "Error", message: "")
                   completionError(error)
                }
        }
    }
    
    static func getPatch(request:PatchServiceRequest, completionSuccess: @escaping(Any?) -> Void, completionError: @escaping (ApiError) -> ())
    {
        let urlString = request.urlString()
        
        let headers = request.getHeaders()
        
        let parameters = request.getParameters()
        
        AF.request(urlString, method: .patch, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                switch response.result {
                case .success(let json):
                    print("Success with JSON: \(json)")
                    
                    completionSuccess(json)
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    var errorString: String?
                    if let data = response.data {
                        if let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any] {
                            errorString = json["message"] as? String
                        }
                    }
                    
                    print(String(errorString ?? "")) //Contains General error message or specific.
                    if let _ = errorString {
                        completionError(ApiError(message: errorString))
                    } else {
                        completionError(ApiError(message: error.errorDescription))
                    }
                    
                }
            }
    }
    
    static func delete(request:DeleteServiceRequest, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError) -> ())
    {
        let urlString = request.urlString()
                
        let headers = request.getHeaders()
        
        let parameters = request.getParameters()
        
        AF.request(urlString, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.response?.statusCode == 201 || response.response?.statusCode == 200) {
                   completionSuccess()
                } else {
                    if let data = response.data {
                        
                        do{
                            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                                if let error: ApiError = decode(json) {
                                    completionError(error)
                                }
                            }
                        } catch {
                            print("erroMsg")
                            
                            let json = String(data: data, encoding: String.Encoding.utf8)
                            completionError(ApiError(status: nil, error: "Register error", message: json))
                        }
                    }
                    let error = ApiError(status: nil, error: "Error", message: "")
                   completionError(error)
                }
        }
    }
}
