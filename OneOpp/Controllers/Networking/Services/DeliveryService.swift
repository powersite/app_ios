//
//  DeliveryService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 27/10/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct DeliveryService {
//    static let DELIVERY_INFO_PREFIX = "delivery_info_"
    static func postDelivery(delivery: Delivery, completionSucessfully: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        
//        let userDefaults = UserDefaults.standard
//        do {
//            if let email = UserController.shared.user?.email {
//                try userDefaults.setObject(delivery, forKey: DeliveryService.DELIVERY_INFO_PREFIX + email)
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
        UserController.shared.saveDeliveryInfo(delivery: delivery)
        
        let deliveryServiceReq = PostDeliveryServiceRequest()
        deliveryServiceReq.delivery = delivery
        
        NetworkingController.post(request: deliveryServiceReq, completionSuccess: completionSucessfully, completionError: completionError)
    }
    
    static func getDeliveries(completionSucessfully: @escaping([Delivery]?) -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        let deliveryServiceReq = GetDeliveryServiceRequest()
        
        NetworkingController.get(request: deliveryServiceReq, completionSucessfully: { (json) in
            if let json = json {
                let deliveries: [Delivery]? = decode(json)
                completionSucessfully(deliveries)
            } else {
                completionSucessfully([])                
            }
        }) { (error) in
            print(error.message ?? "")
            completionError(error)
        }
    }
    
}
