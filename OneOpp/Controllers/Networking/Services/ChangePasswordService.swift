//
//  ChangePasswordService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 12/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct ChangePasswordService {
    static func changePass(pass: String, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        let changePassServiceReq = ChangePasswordServiceRequest()
        changePassServiceReq.pass = pass
        
        NetworkingController.post(request: changePassServiceReq, completionSuccess: completionSuccess, completionError: completionError)
    }
} 
