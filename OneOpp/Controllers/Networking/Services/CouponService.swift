//
//  CouponService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 26/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct CouponService {
    enum Status: String {
        case ACTIVE
        case INACTIVE
    }
    
    static func getCount(status: Status = .ACTIVE, completionSucessfully: @escaping(Int?) -> Void, completionError: @escaping (String) -> ()) {
        let couponCountServiceReq = GetServiceRequest(serviceName: "coupon/count")
        
        
        NetworkingController.get(request: couponCountServiceReq, completionSucessfully: { (json) in
            if let json = json {
                completionSucessfully(json as? Int)
            }
        }) { (error) in
            print(error.message ?? "")
            completionError(error.message ?? "")
        }
    }
    
    static func getCoupons(status: Status = .ACTIVE, completionSucessfully: @escaping([Coupon]?) -> Void, completionError: @escaping (String) -> ()) {
        let couponServiceReq = GetCouponServiceRequest()
        couponServiceReq.status = status
        
        NetworkingController.get(request: couponServiceReq, completionSucessfully: { (json) in
            if let json = json {
                let coupons: [Coupon]? = decode(json)
                completionSucessfully(coupons)
            }
        }) { (error) in
            print(error.message ?? "")
            completionError(error.message ?? "")
        }
    }
    static func deleteCoupon(couponId: Int, completionSucessfully: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        let serviceName = "coupon/delete/\(couponId)"
        let deleteCouponServiceReq = DeleteServiceRequest(serviceName: serviceName)
        
        NetworkingController.delete(request: deleteCouponServiceReq, completionSuccess: completionSucessfully, completionError: completionError)
    }
}
