//
//  NotificationCategoriesService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 04/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct NotificationCategoriesService {
    static func getNotificationCategories(completionSucessfully: @escaping([Category]?) -> Void, completionError: @escaping (String) -> ()) {
        let categoryServiceReq = PatchCategoriesServiceRequest()
        
        NetworkingController.getPatch(request: categoryServiceReq) { (json) in
            if let json = json {
                let categories: [Category]? = decode(json)
                completionSucessfully(categories)
            }
        } completionError: { (error) in
            print(error.message ?? "")
            completionError(error.message ?? "")
        }
    }
    
    static func addCategoriesToNotification(categories: [Category], completionSuccessfully: @escaping() -> Void, completionError: @escaping (ApiError) -> ()) {
        
        let urlString = NetworkingController.Constants.BASE_URL + "/account/set/categories"
        
        var request = URLRequest(url: URL(string: urlString)!)
        request.httpMethod = "PATCH"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue(UserController.shared.token, forHTTPHeaderField: "Authorization")
        request.setValue("*/*", forHTTPHeaderField: "Accept")
        
        do {
            
            if let httpBody = try? JSONEncoder().encode(categories) {
                request.httpBody = httpBody
            }
        } catch {
            print("JSON serialization failed")
        }

        AF.request(request)
            .validate()
            .responseJSON { (response) -> Void in
                
                if (response.response?.statusCode == 201 || response.response?.statusCode == 200) {
                   completionSuccessfully()
                } else {
                    if let data = response.data {
                        
                        do{
                            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                                if let error: ApiError = decode(json) {
                                    completionError(error)
                                }
                            }
                        } catch {
                            print("erroMsg")
                            
                            let json = String(data: data, encoding: String.Encoding.utf8)
                            completionError(ApiError(status: nil, error: "Register error", message: json))
                        }
                    }
                    let error = ApiError(status: nil, error: "Error", message: "")
                   completionError(error)
                }
        }

    }
}
