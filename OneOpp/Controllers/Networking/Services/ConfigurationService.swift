//
//  ConfigurationService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 14/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation

struct ConfigurationService {
    static func sendConfiguration(allowNotif: Bool, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        let configServiceReq = ConfigurationServiceRequest()
        configServiceReq.benefit = allowNotif
        configServiceReq.general = allowNotif
        
        NetworkingController.patch(request: configServiceReq, completionSuccess: completionSuccess, completionError: completionError)
    }
}
