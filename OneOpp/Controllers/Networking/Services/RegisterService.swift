//
//  RegisterService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 21/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct RegisterService {
    
    static func register(email: String,name: String, password: String, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        let registerServiceReq = RegisterServiceRequest()
        registerServiceReq.email = email
        registerServiceReq.name = name
        registerServiceReq.pass = password
        
//        let headers: HTTPHeaders = ["Accept" : "application/json"]
        NetworkingController.post(request: registerServiceReq, completionSuccess: completionSuccess, completionError: completionError)
    }
}
