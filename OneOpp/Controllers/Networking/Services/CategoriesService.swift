//
//  CategoriesService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 29/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct CategoriesService {
    static func getCategories(completionSucessfully: @escaping([Category]?) -> Void, completionError: @escaping (String) -> ()) {
        let categoryServiceReq = GetServiceRequest(serviceName: "category")
        
        NetworkingController.get(request: categoryServiceReq, completionSucessfully: { (json) in
            if let json = json {
                let categories: [Category]? = decode(json)
                completionSucessfully(categories)
            }
        }) { (error) in
            print(error.message ?? "")
            completionError(error.message ?? "")
        }
    }
}
