//
//  MessageService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 12/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
struct MessageService {
    
    static func postMessage(msg: String, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
        let msgServiceRequest = MessageServiceRequest()
        msgServiceRequest.message = msg
        NetworkingController.post(request: msgServiceRequest, completionSuccess: completionSuccess, completionError: completionError)
    }
    
}
