//
//  LoginService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation

struct LoginService {
    
    static func regularLogin(email: String, password: String, completionSuccess: @escaping(String
        ) -> Void, completionError: @escaping (ApiError?) -> ()) {
        
        let loginServiceReq = LoginServiceRequest()
        loginServiceReq.email = email
        loginServiceReq.pass = password
        
        NetworkingController.loginPost(request: loginServiceReq, completionSuccess: completionSuccess, completionError: completionError)
    }
    
    static func socialLogin(token: String, completionSuccess: @escaping(String
        ) -> Void, completionError: @escaping (ApiError?) -> ()) {
        let loginServiceReq = SocialLoginServiceRequest()
        loginServiceReq.token = token
        
        NetworkingController.socialLoginPost(request: loginServiceReq, completionSuccess: completionSuccess, completionError: completionError)
    }
    
//    static func fcm(fcmToken: String, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {
//        let loginServiceReq = FCMServiceRequest()
//        loginServiceReq.fcmToken = fcmToken
//    
//        NetworkingController.post(request: loginServiceReq, completionSuccess: completionSuccess, completionError: completionError)
//    }
}
