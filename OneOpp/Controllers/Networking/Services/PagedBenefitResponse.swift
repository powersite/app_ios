//
//  PagedBenefitResponse.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation

struct PagedBenefitResponse {
  let benefits: [Benefit]
  let total: Int
//  let hasMore: Bool
//  let page: Int
}
