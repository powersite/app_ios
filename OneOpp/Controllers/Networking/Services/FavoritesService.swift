//
//  FavoritesService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/07/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct FavoritesService {
//    enum OrderBy: String {
//        case DATE
//        case DISCOUNT
//        case GEO
//    }
    
    static func getFavorites(completionSucessfully: @escaping([Benefit]?) -> Void, completionError: @escaping (String) -> ()) {
        let favoritesServiceReq = GetServiceRequest(serviceName: "benefit/favorites")
        
        NetworkingController.get(request: favoritesServiceReq, completionSucessfully: { (json) in
            if let json = json {
                let favorites: [Benefit]? = decode(json)
                completionSucessfully(favorites)
            }
        }) { (error) in
            print(error.message ?? "")
            completionError(error.message ?? "")
        }
    }
    
    static func setFavorite(benefitId: Int, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {

        let setFavoriteServiceReq = PostFavoriteServiceRequest()
        setFavoriteServiceReq.benefitId = benefitId

        NetworkingController.post(request: setFavoriteServiceReq, completionSuccess: completionSuccess, completionError: completionError)
    }
    
    static func deleteFavorite(benefitId: Int, completionSuccess: @escaping() -> Void, completionError: @escaping (ApiError?) -> ()) {

        let deleteFavoriteServiceReq = DeleteFavoriteServiceRequest()
        deleteFavoriteServiceReq.benefitId = benefitId

        NetworkingController.delete(request: deleteFavoriteServiceReq, completionSuccess: completionSuccess, completionError: completionError)
        
    }
}
