//
//  MercadoPagoService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 19/04/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct MercadoPagoService {
    static func createPayment(benefit: Benefit, completionSuccess: @escaping(String) -> Void, completionError: @escaping (ApiError) -> ()) {
        
        let urlString = "https://mercadopago.powersite.com.ar/public/api/oneopp/pay/new"
                
        let headers: HTTPHeaders = ["Accept" : "*/*",
                                    "Content-Type": "application/json"]
        
        var parameters = Dictionary<String,String>()
        parameters["firstname"] = UserController.shared.user?.name
        parameters["lastname"] = UserController.shared.user?.name
        parameters["email"] = UserController.shared.user?.email
        parameters["description"] = benefit.title
        if let business = benefit.business {
            parameters["access_token"] = business.mpAccessToken
        }
        parameters["price"] =  "\(String(describing: benefit.price ?? 0))"
        parameters["id_transaction"] = Date().datetime()
        parameters["id_client"] = "4"
        
        
        let encoding = JSONEncoding.default
        
        AF.request(urlString, method: .post, parameters: parameters, encoding: encoding, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                switch response.result {
                case .success(let json):
                    print("Success with JSON: \(json)")
                    if let jsonDict = json as? [String: Any], let preferenceId = jsonDict["id"] as? String {
                        completionSuccess(preferenceId)
                    } else {
                        let err = ApiError(message: "no id")
                        completionError(err)
                    }
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    var errorString: String?
                    if let data = response.data {
                        if let json = try? JSONSerialization.jsonObject(with: data, options: []) as! [String: Any] {
                            errorString = json["message"] as? String
                        }
                    }
                    
                    print(String(errorString ?? "")) //Contains General error message or specific.
                    if let _ = errorString {
                        completionError(ApiError(message: errorString))
                    } else {
                        completionError(ApiError(message: error.errorDescription))
                    }
                    
                }
        }

    }
}
