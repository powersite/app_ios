//
//  BenefitService.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 26/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire
import Argo

struct BenefitService {
    enum OrderBy: String {
        case DATE
        case DISCOUNT
        case GEO
    }
    
    static func getBenefits(req: GetBenefitServiceRequest, page: Int, completionSucessfully: @escaping(PagedBenefitResponse) -> Void, completionError: @escaping (String) -> ()) {
//        let beneficioServiceReq = GetBenefitServiceRequest()
//        beneficioServiceReq.page = page
//        beneficioServiceReq.size = size
//        beneficioServiceReq.orderBy = orderedBy
//        beneficioServiceReq.search_term = searchTerm
//        beneficioServiceReq.category_id = category_id
        req.page = page
        
        let urlString = req.urlString()
        let headers = req.getHeaders()
        let parameters = req.getParameters()
        //        let encoding = beneficioServiceReq.getEncoding()
        
        
        AF.request(urlString, parameters: parameters, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                switch response.result {
                case .success(let json):
                    print("Success with JSON: \(json)")
                    if let headerFields = response.response?.allHeaderFields {
                        if let totalS = headerFields["X-Total-Count"] as? String, let total = Int(totalS), let benefits: [Benefit] = decode(json) {
                            let pagedBenefitResponse = PagedBenefitResponse(benefits: benefits, total: total)
                            completionSucessfully(pagedBenefitResponse)
                        }
                        print(headerFields)
                    }
                    
                case .failure(let error):
                    print("Request failed with error: \(error)")
                    completionError(error.errorDescription ?? "")
                    
                }
        }
    }
    
    static func getBenefitById( _ benefitId: String, completionSucessfully: @escaping(Benefit?) -> Void, completionError: @escaping (String) -> ()) {
        
        let serviceName = "benefit/\(benefitId)"
        let reserveBenefitServiceReq = GetServiceRequest(serviceName: serviceName)
        
        NetworkingController.get(request: reserveBenefitServiceReq, completionSucessfully: { (json) in
            if let json = json {
                let coupon: Benefit? = decode(json)
                completionSucessfully(coupon)
            }
        }) { (error) in
            print(error.message ?? "")
            completionError(error.message ?? "")
        }
    }
    
    static func reserveBenefit(benefitId: Int, completionSucessfully: @escaping(Coupon?) -> Void, completionError: @escaping (String) -> ()) {
        
        let serviceName = "benefit/\(benefitId)/reserve"
        let reserveBenefitServiceReq = GetServiceRequest(serviceName: serviceName)
        
        NetworkingController.get(request: reserveBenefitServiceReq, completionSucessfully: { (json) in
            if let json = json {
                let coupon: Coupon? = decode(json)
                completionSucessfully(coupon)
            }
        }) { (error) in
            print(error.message ?? "")
            completionError(error.message ?? "")
        }
    }
}
