//
//  ConfigurationServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 14/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class ConfigurationServiceRequest: PatchServiceRequest {
    var benefit: Bool?
    var general: Bool?
    
    init()
    {
        super.init(serviceName: "account/configuration")
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["notificationBenefit"] = self.benefit
        params["notificationGeneral"] = self.general
        
        return params
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getHeaders() -> HTTPHeaders {
            
        var headers = self.getCommonHeaders()
        headers["Content-Type"] = "application/json"
        return headers
    }
}
