//
//  ChangePasswordServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 12/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class ChangePasswordServiceRequest: PostServiceRequest {
    var pass: String?
    
    init()
    {
        super.init(serviceName: "account/changepassword")
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["password"] = self.pass
        
        return params
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getEncoding() -> ParameterEncoding {
        return URLEncoding.default
    }
    
//    override func getHeaders() -> HTTPHeaders {
//        var headers = self.getCommonHeaders()
//        headers["Content-Type"] = "application/json"
//        return headers
//    }
}
