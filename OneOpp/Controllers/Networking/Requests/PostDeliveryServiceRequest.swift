//
//  DeliveryServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 27/10/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class PostDeliveryServiceRequest: PostServiceRequest {
    var delivery: Delivery?
    
    init()
    {
        super.init(serviceName: "delivery")
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["benefitId"] = delivery?.benefitId
        params["betweenStreet"] = delivery?.betweenStreet
        params["businessId"] = delivery?.businessId
        params["businessObservation"] = delivery?.businessObservation
        params["deliveryStatusId"] = delivery?.deliveryStatusId
        params["deliveryStatusName"] = delivery?.deliveryStatusName
        params["department"] = delivery?.department
        params["deviceToken"] = delivery?.deviceToken
        params["email"] = delivery?.email
        params["extraData"] = delivery?.extraData
        params["floor"] = delivery?.floor
        params["id"] = delivery?.id
        params["name"] = delivery?.name
        params["number"] = delivery?.number
        params["phone"] = delivery?.phone
        params["street"] = delivery?.street
        
        return params
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
}
