//
//  LoginServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class LoginServiceRequest: PostServiceRequest {
    var email: String?
    var pass: String?
    
    init()
    {
        super.init(serviceName: "login")
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["email"] = self.email
        params["password"] = self.pass
        
        return params
    }
}
