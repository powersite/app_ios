//
//  FCMServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 03/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class FCMServiceRequest: PostServiceRequest {
    var fcmToken: String?
    
    init()
    {
        super.init(serviceName: "account/fcm")
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["token"] = self.fcmToken
        
        return params
    }
    
    override func getEncoding() -> ParameterEncoding {
        return URLEncoding.default
    }
}
