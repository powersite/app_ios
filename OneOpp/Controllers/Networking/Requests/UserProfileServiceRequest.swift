//
//  UserProfileServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class UserProfileServiceRequest: GetServiceRequest {
    var email: String?
    var pass: String?
    
    init()
    {
        super.init(serviceName: "account/profile")
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    override func getHeaders() -> HTTPHeaders {
        
        var headers = self.getCommonHeaders()
//        if let token = UserController.shared.token {//NetworkingController.shared.access_token {
//
//            headers["Authorization"] = "Bearer " + token
//
//        }
        return headers
    }
}
