//
//  PostServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 25/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Alamofire

class PostServiceRequest: ServiceRequest
{
//   func commonParameters() -> Dictionary<String,String>
//    {
//        var params = Dictionary<String,String>()
//
//        return params
//    }
    
    override func getCommonBodyParameters() -> Dictionary<String, Any> {
        let params = super.getCommonBodyParameters()
        return params
    }
    
    override func getHeaders() -> HTTPHeaders
    {
        return getCommonHeaders()
    }
}
