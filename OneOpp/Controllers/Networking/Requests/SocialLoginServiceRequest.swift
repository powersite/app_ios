//
//  SocialLoginServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
class SocialLoginServiceRequest: PostServiceRequest {
    
    var token: String?
    
    init()
    {
        super.init(serviceName: "login/social")
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["token"] = self.token
        
        return params
    }
}
