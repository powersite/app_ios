//
//  ServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Alamofire

class ServiceRequest
{
    private(set) public var serviceName:String
    var body:[String:String]?
    
    static var basePath:String!
    static var appName:String!
    static var apiKey:String!
    
    init(serviceName:String)
    {
        self.serviceName = serviceName
    }
    
    func getEncoding() -> ParameterEncoding {
        return JSONEncoding.default
    }
    
    func urlString() -> String
    {
        return NetworkingController.Constants.BASE_URL + "/\(serviceName)"
    }
    
    func requiresAuthorization() -> Bool {
        return false
    }
    
    func getParameters() -> Dictionary<String,Any>
    {
        fatalError("Subclasses need to implement the `getParameters()` method.")
    }
    
    func getHeaders() -> HTTPHeaders
    {
        fatalError("Subclasses need to implement the `getHeaders()` method.")
    }
    
    func getCommonBodyParameters() -> Dictionary<String,Any>
    {
        let params = Dictionary<String,String>()
        
        return params
    }
    
    func getBodyParameters() -> Dictionary<String,Any> {
        fatalError("Subclasses need to implement the `getBodyParameters()` method.")
    }
    
    func getCommonHeaders() -> HTTPHeaders
    {
        var headers: HTTPHeaders = [
            
            "Accept" : "*/*"//"application/json",
//            "Content-Type" : "application/json"
            
        ]
        
        if requiresAuthorization(), let token = UserController.shared.token {//NetworkingController.shared.access_token {
            
            headers["Authorization"] = token
            
        } 
        
        return headers
    }
    
}
