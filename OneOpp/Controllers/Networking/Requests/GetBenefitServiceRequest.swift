//
//  GetBenefitServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 27/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class GetBenefitServiceRequest: GetServiceRequest {
    
    var page: Int!
    var size: Int!
    var orderBy: BenefitService.OrderBy!
    var latitud: String?
    var longitud: String?
    var category_id: Int?
    var search_term: String?
    
    init()
    {
        super.init(serviceName: "benefit")
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getHeaders() -> HTTPHeaders {
        return self.getCommonHeaders()
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getParameters()
        
        params["page"] = self.page
        params["size"] = self.size
        params["order_by"] = self.orderBy.rawValue
        
        if let lat = self.latitud {
            params["latitud"] = lat
        }
        
        if let long = self.longitud {
            params["longitud"] = long
        }
        
        if let catId = self.category_id {
            params["category_id"] = catId
        }
        
        if let searchTerm = self.search_term {
            if !searchTerm.isEmpty {
                params["search_term"] = searchTerm
            }
        }
        
        return params
    }
}
