//
//  GetServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Alamofire

class GetServiceRequest: ServiceRequest
{
    
    /*override func urlString() -> String
    {
        let finalPath = "https://\(ServiceRequest.basePath!)" + "/sites/\(Settings.shared.appName!)/_api/web/lists/getbytitle('\(serviceName)')/items?$top=1000"
        ///sites/{{app_name}}/_api/web/lists/getbytitle('Products')/items?$select=product_name&$select=brand
        
        return finalPath
    }*/
    override func getHeaders() -> HTTPHeaders {
        return self.getCommonHeaders()
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        let parameters = super.getCommonBodyParameters()
        return parameters
    }
    
}
