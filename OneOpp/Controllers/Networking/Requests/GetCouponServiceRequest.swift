//
//  GetCouponServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 27/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class GetCouponServiceRequest: GetServiceRequest {
    
    var status: CouponService.Status!
    
    init()
    {
        super.init(serviceName: "coupon/self")
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getHeaders() -> HTTPHeaders {
        return self.getCommonHeaders()
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getParameters()
        
        params["status"] = self.status
        
        return params
    }
}
