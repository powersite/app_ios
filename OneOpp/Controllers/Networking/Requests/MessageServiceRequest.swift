//
//  MessageServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 12/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class MessageServiceRequest: PostServiceRequest {
    var message: String?
    
    init()
    {
        super.init(serviceName: "message")
    }
    
    override func getEncoding() -> ParameterEncoding {
        guard let s = self.message else {
            return super.getEncoding()
        }
        return s
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["message"] = self.message
        
        return params
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getHeaders() -> HTTPHeaders {
            
            var headers = self.getCommonHeaders()
            headers["Content-Type"] = "application/json"
            return headers
    }
}
