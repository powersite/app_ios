//
//  GetBlackListServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 04/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class GetBlackListServiceRequest: GetServiceRequest {
    
//    var status: CouponService.Status!
    
    init()
    {
        super.init(serviceName: "account/black/list/business")
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getHeaders() -> HTTPHeaders {
        return self.getCommonHeaders()
    }
}
