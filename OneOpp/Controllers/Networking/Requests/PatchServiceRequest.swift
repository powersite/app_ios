//
//  PatchServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 14/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Alamofire

class PatchServiceRequest: ServiceRequest
{
    var parameterList: [Any]?
    
    override func getCommonBodyParameters() -> Dictionary<String, Any> {
        let params = super.getCommonBodyParameters()
        return params
    }
    
    override func getHeaders() -> HTTPHeaders
    {
        return getCommonHeaders()
    }
}
