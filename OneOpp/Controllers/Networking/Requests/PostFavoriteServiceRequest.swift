//
//  PostFavoriteServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 01/08/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class PostFavoriteServiceRequest: PostServiceRequest {
    var benefitId: Int?
    
    init()
    {
        super.init(serviceName: "benefit/favorites")
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        var params = super.getCommonBodyParameters()
        
        params["id"] = benefitId
        return params
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
}
