//
//  PatchSetCategoriesServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 15/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
class PatchSetCategoriesServiceRequest: PatchServiceRequest {
    var categories: [Category]?
    init()
    {
        super.init(serviceName: "account/set/categories")
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        let params = Dictionary<String,String>()
        
        return params
    }
}

