//
//  GetDeliveryServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 13/01/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class GetDeliveryServiceRequest: GetServiceRequest {
    
    
    init()
    {
        super.init(serviceName: "delivery/self")
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
}
