//
//  PatchCategoriesServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 04/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation

class PatchCategoriesServiceRequest: PatchServiceRequest {
    init()
    {
        super.init(serviceName: "account/categories")
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
    
    override func getParameters() -> Dictionary<String, Any> {
        
        let params = Dictionary<String,String>()
        
        return params
    }
}

