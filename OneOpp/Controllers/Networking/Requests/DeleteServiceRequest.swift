//
//  DeleteServiceRequest.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 09/07/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Alamofire

class DeleteServiceRequest: ServiceRequest
{
    
    override func getCommonBodyParameters() -> Dictionary<String, Any> {
        let params = super.getCommonBodyParameters()
        return params
    }
    
    override func getHeaders() -> HTTPHeaders
    {
        return getCommonHeaders()
    }
    
    override func getParameters() -> Dictionary<String,Any>
    {
        let params = super.getCommonBodyParameters()
        return params
    }
    
    override func requiresAuthorization() -> Bool {
        return true
    }
}
