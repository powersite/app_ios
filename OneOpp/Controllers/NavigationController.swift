//
//  NavigationController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 30/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: NSObject {
    static let shared = NavigationController()
    static let onboardingId = "onboarding_show"
    
    private override init() {
        
        super.init()
        self.onboardingShown = UserDefaults.standard.bool(forKey: NavigationController.onboardingId)
    }
    
    var onboardingShown: Bool?
    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    func showLogInScreen() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let nc = sb.instantiateViewController(withIdentifier: "MainNavigationController") as? UINavigationController {
            let c = sb.instantiateViewController(withIdentifier: "LoginViewController")
            nc.addChild(c)
            self.appDelegate?.window!.rootViewController = nc
        }
    }
    
    func showOnboarding() {
        //"OnboardingScreen"
        UserDefaults.standard.set(true, forKey: NavigationController.onboardingId)
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "OnboardingScreen") as? OnboardingCollectionViewController
        (self.appDelegate?.window?.rootViewController as? UINavigationController)?.viewControllers = [vc!]
    }

    func showMainLoggedInNavigationScreen() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let nc = sb.instantiateViewController(withIdentifier: "MainNavigationController") as? UINavigationController {
            let c = sb.instantiateViewController(withIdentifier: "LoggedInMainScreen")
            nc.addChild(c)
            self.appDelegate?.window!.rootViewController = nc
        }
    }
    
    func showBenefitDetail( _ benefit: Benefit) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let nc = sb.instantiateViewController(withIdentifier: "MainNavigationController") as? UINavigationController {
            let c = sb.instantiateViewController(withIdentifier: "LoggedInMainScreen")
            nc.addChild(c)
            self.appDelegate?.window!.rootViewController = nc
            if let benefitDetailVC = sb.instantiateViewController(withIdentifier: "BenefitDetail") as? BenefitDetailViewController {
                benefitDetailVC.benefit = benefit
                nc.pushViewController(benefitDetailVC, animated: true)
            }
        }
    }
    
    static func showDocument(from: UINavigationController, for type: DocumentsController.DocumentType) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let documentVC = storyboard.instantiateViewController(withIdentifier: "Document") as? DocumentViewController {
            documentVC.docType = type
            from.pushViewController(documentVC, animated: true)
        }
    }
    
    static func showFAQ(from: UINavigationController) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let faqVC = storyboard.instantiateViewController(withIdentifier: "FAQ") as? FAQTableViewController {
            from.pushViewController(faqVC, animated: true)
        }
    }
    
    static func showContact(from: UINavigationController) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let contactVC = storyboard.instantiateViewController(withIdentifier: "ContactVC") as? ContactViewController {
            from.pushViewController(contactVC, animated: true)
        }
    }
    
    static func returnToMain(nav: UINavigationController, onCompletion: (() -> Void)? = nil) {
        var mainVc: UIViewController?
        for vc in nav.viewControllers {
            if vc.isKind(of: MainScreenViewController.self) {
                mainVc = vc
                break
            }
        }
        if let popVC = mainVc {
            nav.popToViewController(popVC, animated: true)
        }
        onCompletion?()
    }
    
    static func navigateToAddCommerce() {
        if let url = URL(string: Utils.Constants.COMMERCE_URL) {
            UIApplication.shared.open(url)
        }
    }
    
    static func navigateFromTabBarVC(_ from: UIViewController, vc: UIViewController) {
        if let parent = from.parent, let navC = parent.navigationController {
            for child in navC.children {
                if let main = child as? MainScreenViewController {
                    main.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
//    func showMyAccount(vc: UIViewController) {
//        if let mainVC =  vc.navigationController?.tabBarController?.parent as? MainScreenViewController {
//            mainVC.hideSideMenu()
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let profileVC = storyboard.instantiateViewController(withIdentifier: "ProfileViewController")
//            currentActiveNav.pushViewController(profileVC, animated: true)
//        }
//        
//    }
}
