//
//  BusinessesController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 05/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
struct BusinessesController {
    static func getBlackListedBusinesses(completionSuccessfull: @escaping([Business]?) -> Void, completionError: @escaping() -> Void) {
        
        var allBusinesses: [Business]?
        var blackedBusinesses: [Business]?
        
        let group = DispatchGroup()
        
        group.enter()
        BusinessService.getBusinesses { (businesses) in
            allBusinesses = businesses
            group.leave()
        } completionError: { (error) in
            group.leave()
        }
        
        group.enter()
        BusinessService.getBlackListBusinesses { (businesses) in
            blackedBusinesses = businesses
            group.leave()
        } completionError: { (error) in
            group.leave()
        }
        
        group.notify(queue: .main) {
            if let blackedBusinesses = blackedBusinesses {
                let allBusinessesWithBlackeListInfo = allBusinesses?.map{ (business) -> Business in
                    var newBusiness = business
                    if blackedBusinesses.contains(where: { (blackedBusiness) -> Bool in
                        return blackedBusiness.id == business.id
                    }) {
                        newBusiness.blackListed = true
                    }
                    return newBusiness
                }
                completionSuccessfull(allBusinessesWithBlackeListInfo)
            } else {
                if let allBusinesses = allBusinesses {
                    completionSuccessfull(allBusinesses)
                } else {
                    completionError()
                }
            }
            
        }
    }
    
}
