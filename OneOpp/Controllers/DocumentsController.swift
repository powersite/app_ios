//
//  DocumentsController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 09/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
struct DocumentsController {
    enum DocumentType {
        case tyc
        case policy
    }
    
    let tycContent = ""
    static func loadContentForDocument(_ type: DocumentType) -> String? {
        var title: String?
        switch type {
        case .tyc:
            title = "TerminosyCondiciones"
        case .policy:
            title = "PrivacyPolicy"
        default:
            fatalError("Debería responder a uno de los tipos listados")
        }
        if let path = Bundle.main.path(forResource: title!, ofType: "txt") {
            do {
                let contentString = try String(contentsOfFile: path, encoding: .utf8)
                return contentString
            } catch {
                print("File not found")
            }
        }
        return nil
    }
}
