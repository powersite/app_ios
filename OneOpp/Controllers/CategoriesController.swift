//
//  CategoriesController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 20/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo

struct CategoriesController {
    static func getAuthorizedCategories(completionSuccessfull: @escaping([Category]?) -> Void, completionError: @escaping() -> Void) {
        
        var allCategories: [Category]?
        var userCategories: [Category]?
        
        let group = DispatchGroup()
        Utils.startProgressAnimation()
        
        group.enter()
        // all categories
        CategoriesService.getCategories { (categproes) in
            allCategories = categproes
            group.leave()
        } completionError: { (_) in
            print("error getting all categories")
            group.leave()
        }

        
        group.enter()
        // user categories
        NotificationCategoriesService.getNotificationCategories(completionSucessfully: { (categories) in
            userCategories = categories
            group.leave()
        }, completionError: { (err) in
            print(err.debugDescription)
            group.leave()
        })
        
        group.notify(queue: .main){
            if let userCategories = userCategories {
                let allCategoriesWithAllowabilityInfo = allCategories?.map{ (category) -> Category in
                    var newCategory = category
                    if userCategories.contains(where: { (allowedCategory) -> Bool in
                        return allowedCategory.id == category.id
                    }) {
                        newCategory.allowed = true
                    }
                    return newCategory
                }
                completionSuccessfull(allCategoriesWithAllowabilityInfo)
            } else {
                if let allCategories = allCategories {
                    completionSuccessfull(allCategories)
                } else {
                    completionError()
                }
            }
        }
    }
    
    private func getUserCategories(completionSucessfully: @escaping([Category]?) -> Void, completionError: @escaping (String) -> ()) {
        let categoryServiceReq = PatchCategoriesServiceRequest()
        
        NetworkingController.getPatch(request: categoryServiceReq) { (json) in
            if let json = json {
                let categories: [Category]? = decode(json)
                completionSucessfully(categories)
            }
        } completionError: { (error) in
            print(error.message ?? "")
            completionError(error.message ?? "")
        }
    }
}
