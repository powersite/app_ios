//
//  UITextField.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 11/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

private var rightViews = NSMapTable<UITextField, UIView>(keyOptions: NSPointerFunctions.Options.weakMemory, valueOptions: NSPointerFunctions.Options.strongMemory)
private var errorViews = NSMapTable<UITextField, UIView>(keyOptions: NSPointerFunctions.Options.weakMemory, valueOptions: NSPointerFunctions.Options.strongMemory)

extension UITextField {
    func setPasswordEye() {
        if let rightView = rightView, rightView.tag != 998 {
            rightViews.setObject(rightView, forKey: self)
        }
        
        let passEye = UIButton(type: .custom)
        passEye.tag = 998
        passEye.setImage(UIImage(named: "show-pass"), for: .normal)
        passEye.frame = CGRect(x: 0, y: 0, width: frame.size.height, height: frame.size.height)
        passEye.addTarget(self, action: #selector(togglePass), for: .touchUpInside)
        passEye.widthAnchor.constraint(equalToConstant: 25).isActive = true
        passEye.heightAnchor.constraint(equalToConstant: 25).isActive = true
        passEye.isEnabled = (self.text?.count ?? 0) > 0
        rightView = passEye
        rightViewMode = .always

        // Save view with error message
//        errorViews.setObject(container, forKey: self)
    }
           
    @objc func togglePass(_ sender: UIButton) {
        self.isSecureTextEntry.toggle()
       if self.isSecureTextEntry {
           sender.setImage(UIImage(named: "show-pass"), for: .normal)
       } else {
           sender.setImage(UIImage(named: "hide-pass"), for: .normal)
       }
   }
    
    // Add/remove error message
    func setError(_ string: String? = nil, show: Bool = true) {
        if let rightView = rightView, rightView.tag != 999 {
            rightViews.setObject(rightView, forKey: self)
        }

        // Remove message
        guard string != nil else {
            if let rightView = rightViews.object(forKey: self) {
                self.rightView = rightView
                rightViews.removeObject(forKey: self)
            } else {
                self.rightView = nil
            }

            if let errorView = errorViews.object(forKey: self) {
                errorView.isHidden = true
                errorViews.removeObject(forKey: self)
            }

            return
        }

        // Create container
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false

        // Create triangle
        let triangle = TriangleTop()
        triangle.backgroundColor = .clear
        triangle.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(triangle)

        // Create red line
        let line = UIView()
        line.backgroundColor = .red
        line.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(line)
        
        let blackBox = UIView()
        blackBox.backgroundColor = .black
        blackBox.translatesAutoresizingMaskIntoConstraints = false
        

        // Create message
        let label = UILabel()
        label.text = string
        label.textColor = .white
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 12)
        label.backgroundColor = .clear
        label.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 250), for: .horizontal)
        label.translatesAutoresizingMaskIntoConstraints = false
        
//        container.addSubview(label)
        blackBox.addSubview(label)
        
        // Set constraints for label
        label.topAnchor.constraint(equalTo: blackBox.topAnchor, constant: 5).isActive = true
        blackBox.bottomAnchor.constraint(equalTo: label.bottomAnchor, constant: 5).isActive = true
        label.leadingAnchor.constraint(equalTo: blackBox.leadingAnchor, constant: 10).isActive = true
        blackBox.trailingAnchor.constraint(equalTo: label.trailingAnchor, constant: 10).isActive = true
        
        container.addSubview(blackBox)

        // Set constraints for triangle
        triangle.heightAnchor.constraint(equalToConstant: 10).isActive = true
        triangle.widthAnchor.constraint(equalToConstant: 15).isActive = true
        triangle.topAnchor.constraint(equalTo: container.topAnchor, constant: -10).isActive = true
        triangle.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -15).isActive = true

        // Set constraints for line
        line.heightAnchor.constraint(equalToConstant: 3).isActive = true
        line.topAnchor.constraint(equalTo: triangle.bottomAnchor, constant: 0).isActive = true
        
        
        // Set constraints for blackBox
        line.bottomAnchor.constraint(equalToSystemSpacingBelow: blackBox.topAnchor, multiplier: 0).isActive = true
        container.bottomAnchor.constraint(equalToSystemSpacingBelow: blackBox.bottomAnchor, multiplier: 0).isActive = true
        container.leadingAnchor.constraint(equalTo: blackBox.leadingAnchor, constant: 0).isActive = true
        container.trailingAnchor.constraint(equalTo: blackBox.trailingAnchor, constant: 0).isActive = true
        
        line.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 0).isActive = true
        line.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: 0).isActive = true
        
        if !show {
            container.isHidden = true
        }
        // superview!.superview!.addSubview(container)
        UIApplication.shared.keyWindow!.addSubview(container)

        // Set constraints for container
        container.widthAnchor.constraint(lessThanOrEqualTo: superview!.widthAnchor, multiplier: 1).isActive = true
        container.trailingAnchor.constraint(equalTo: superview!.trailingAnchor, constant: 0).isActive = true
        container.topAnchor.constraint(equalTo: superview!.bottomAnchor, constant: 0).isActive = true

        // Hide other error messages
        let enumerator = errorViews.objectEnumerator()
        while let view = enumerator!.nextObject() as! UIView? {
            view.isHidden = true
        }

        // Add right button to textField
        let errorButton = UIButton(type: .custom)
        errorButton.tag = 999
        errorButton.setImage(UIImage(named: "alert-icon"), for: .normal)
        errorButton.frame = CGRect(x: 0, y: 0, width: frame.size.height, height: frame.size.height)
        errorButton.addTarget(self, action: #selector(errorAction), for: .touchUpInside)
        errorButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
        errorButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
        rightView = errorButton
        rightViewMode = .always

        // Save view with error message
        errorViews.setObject(container, forKey: self)
    }

    // Show error message
    @IBAction
    func errorAction(_ sender: Any) {
        let errorButton = sender as! UIButton
        let textField = errorButton.superview as! UITextField

        let errorView = errorViews.object(forKey: textField)
        if let errorView = errorView {
            errorView.isHidden.toggle()
        }

        let enumerator = errorViews.objectEnumerator()
        while let view = enumerator!.nextObject() as! UIView? {
            if view != errorView {
                view.isHidden = true
            }
        }

        // Don't hide keyboard after click by icon
//        UIViewController.isCatchTappedAround = false
    }
}

class TriangleTop: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        context.beginPath()
        context.move(to: CGPoint(x: (rect.maxX / 2.0), y: rect.minY))
        context.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        context.addLine(to: CGPoint(x: (rect.minX / 2.0), y: rect.maxY))
        context.closePath()

        context.setFillColor(UIColor.red.cgColor)
        context.fillPath()
    }
}
