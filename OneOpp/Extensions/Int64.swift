//
//  Int64.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 07/09/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation

extension Int64 {

    func dateFromMiliseconds() -> Date? {
        return Date(milliseconds: self)
    }

}
