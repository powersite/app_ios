//
//  Date.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 03/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
extension Date {
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
    
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter.string(from: self)
    }
    
    func toStringWithTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm"
        return formatter.string(from: self)
    }
    
    func datetime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyhhmm"
        return formatter.string(from: self)
    }
    
    func offsetFrom(date: Date) -> String {

        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self)

        let s = difference.second
        let seconds = "\(s ?? 0) seg"
        let m = difference.minute
        let minutes = "\(m ?? 0) \((m ?? 0) == 1 ? "minuto" : "minutos")" + " " //+ seconds
        let h = difference.hour
        let hours = "\(h ?? 0) \((h ?? 0) == 1 ? "hora" : "horas")" + " " //+ minutes
        let d = difference.day
        let days = "\(d ?? 0) \((d ?? 0) == 1 ? "día" : "días")" + " " //+ hours
        
        let str = "\((((d ?? 0) > 0) ? days : "") + ((h ?? 0) > 0 ? hours : "") + ((m ?? 0) > 0 ? minutes : "") + ((s ?? 0) > 0 ? seconds : ""))"

//        if let day = difference.day, day          > 0 { return days }
//        if let hour = difference.hour, hour       > 0 { return hours }
//        if let minute = difference.minute, minute > 0 { return minutes }
//        if let second = difference.second, second > 0 { return seconds }
//        return ""
        return str
    }
}
