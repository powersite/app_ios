//
//  UIColor+OneOpp.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

extension UIColor{
    convenience init?(hexString: String) {
        let completedHexString = "\(hexString)ff"
        let r, g, b, a: CGFloat
        if completedHexString.hasPrefix("#") {
            let start = completedHexString.index(completedHexString.startIndex, offsetBy: 1)
            let hexColor = completedHexString[start...]
            if hexColor.count == 8 {
                let scanner = Scanner(string: String(hexColor))
                var hexNumber: UInt64 = 0
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        return nil
    }
}
