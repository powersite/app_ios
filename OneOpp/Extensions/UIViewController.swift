//
//  UIViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 02/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String?, msg: String?, style: UIAlertController.Style = .alert, onOK: (() -> ())? = nil) {
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: style)
        
        let OKAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            if let doOnOK = onOK {
                doOnOK()
            }
        }
        alertController.addAction(OKAction)
        present(alertController, animated: true)
    }
    
    func setupNavigationBarAppearance() {
        let navBar = self.navigationController?.navigationBar
        let navigationBarBackground = UIImage(named: "header")?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 70.0, right: -50.0), resizingMode: .tile)
        navBar?.setBackgroundImage(navigationBarBackground, for: .default)
        navBar?.tintColor = .white
        let navbarTitleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor.white]
        navBar?.titleTextAttributes = navbarTitleTextAttributes
    }
}
