//
//  UIView.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 22/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

extension UIView{
    @IBInspectable var borderWidth: CGFloat {
      get {
        return layer.borderWidth
      }
      set(newValue) {
        layer.borderWidth = newValue
      }
    }

    @IBInspectable var borderColor: UIColor? {
      get {
        if let color = layer.borderColor {
            return UIColor(cgColor: color)
        }
        return nil
      }
      set(newValue) {
        layer.borderColor = newValue?.cgColor
        layer.masksToBounds = true
      }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set(newValue) {
          layer.cornerRadius = newValue
        }
    }
    
    func addGradientBackground(_ first: String, _ second: String){
        
        if let firstColor = UIColor(hexString: first), let secondColor = UIColor(hexString: second) {
        
            clipsToBounds = true
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
            gradientLayer.frame = self.bounds
            gradientLayer.startPoint = CGPoint(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint(x: 0, y: 1)
            print(gradientLayer.frame)
            if let sublayers = self.layer.sublayers, sublayers.count > 1 {
                self.layer.replaceSublayer(sublayers.first!, with: gradientLayer)
            } else {
                self.layer.insertSublayer(gradientLayer, at: 0)
            }
        }
     
        
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        self.layer.mask = mask
        self.layer.cornerRadius = radius
        self.layer.maskedCorners = [.layerMinXMinYCorner,
                                    .layerMaxXMinYCorner,
                                    .layerMinXMaxYCorner,
                                    .layerMaxXMaxYCorner]
        self.layer.masksToBounds = true
    }
}
