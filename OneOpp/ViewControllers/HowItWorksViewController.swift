//
//  HowItWorksViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 07/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class HowItWorksViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    @IBOutlet var tabBar: UITabBar!
    
    let steps = [
        Item(image: "step-1", description: "Buscá en OneOpp los descuentos que te ofrecemos."),
        Item(image: "step-2", description: "Selecciona el que más te gusta."),
        Item(image: "step-3", description: "Acercate al negocio y mostrá el código del descuento."),
        Item(image: "step-4", description: "Intenta con otros cupones las veces que quieras. ES GRATIS!!!")
    ]

    @IBOutlet var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tabBar.delegate = self
        self.tabBar.selectedItem = self.tabBar.items?.first
        self.collectionView.reloadData()
//        self.styleSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Skinning.styleHowToTabBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        Skinning.styleMainTabBar()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = collectionView.bounds.size
        return cellSize
    }

}

extension HowItWorksViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return steps.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StepCell", for: indexPath) as? StepCollectionViewCell else {
            fatalError("Debería encontrar la celda")
        }
        let step = steps[indexPath.row]
        guard let image = step.image, let desc = step.description else {
            fatalError("Chequear campos del arreglo")
        }
        
        cell.stepImage.image = UIImage(named: image)
        cell.stepDescription.text = desc
        return cell
    }
    
    
}
extension HowItWorksViewController: UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let ix = tabBar.items?.index(of: item) {
            print(ix)
            
            let ip = IndexPath(row: ix, section: 0)
            self.collectionView.selectItem(at: ip, animated: true, scrollPosition: .centeredHorizontally)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentIndex:CGFloat = self.collectionView.contentOffset.x / self.collectionView.frame.size.width
        self.tabBar.selectedItem = self.tabBar.items?[Int(currentIndex)]
    }
}
