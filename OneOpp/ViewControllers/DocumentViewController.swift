//
//  DocumentViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 09/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class DocumentViewController: UIViewController {

    public var docType: DocumentsController.DocumentType?
    @IBOutlet var contentDoc: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        switch docType {
        case .tyc:
            self.title = "Términos y Condiciones"
        case .policy:
            self.title = "Política de privacidad"
        default:
            print("Debería ser uno de los listados")
        }
        
        self.contentDoc.text = DocumentsController.loadContentForDocument(docType!)
        // Do any additional setup after loading the view.
    }
    

    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()

        self.contentDoc.contentOffset = .zero
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
