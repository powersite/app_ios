//
//  ContactViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 10/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {

    @IBOutlet var textView: UITextView!
    private let placeholder = "Escribí tu inquietud y nos pondremos en contacto."
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textView.delegate = self
        configureTextViewAppearance()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendClicked(_ sender: Any) {
        guard let msg = textView.text, !msg.isEmpty, textView.textColor != .lightGray else {
            return
        }
        Utils.startProgressAnimation()
        MessageService.postMessage(msg: msg, completionSuccess: {
            Utils.stopProgressAnimation()
            self.performSegue(withIdentifier: "ShowContactResult", sender: nil)
        }) { (error) in
            Utils.stopProgressAnimation()
            self.showAlert(title: "Error", msg: error?.message)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func configureTextViewAppearance() {
        textView.text = placeholder
        textView.textColor = .lightGray
        
        //textView.becomeFirstResponder()

        textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
    }

}

extension
ContactViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = .lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
         // Combine the textView text and the replacement text to
           // create the updated text string
           let currentText:String = textView.text
           let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

           // If updated text view will be empty, add the placeholder
           // and set the cursor to the beginning of the text view
           if updatedText.isEmpty {

               textView.text = placeholder
               textView.textColor = .lightGray

               textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
           }

           // Else if the text view's placeholder is showing and the
           // length of the replacement string is greater than 0, set
           // the text color to black then set its text to the
           // replacement string
            else if textView.textColor == .lightGray && !text.isEmpty {
               textView.textColor = .black
               textView.text = text
           }

           // For every other case, the text should change with the usual
           // behavior...
           else {
               return true
           }

           // ...otherwise return false since the updates have already
           // been made
           return false
    }
}
