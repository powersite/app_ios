//
//  FavoritesViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 23/07/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class FavoritesViewController: UIViewController {

    @IBOutlet var mainStack: UIStackView!
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    private var favorites: [Benefit]?
    
    private var shouldShowLoadingCell = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        `tableView`.register(UINib(nibName: "BenefitTableViewCell", bundle: nil), forCellReuseIdentifier: BenefitTableViewCell.benefitId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("didAppear")
        secondView.isHidden = false
        firstView.isHidden = true
        self.loadFavorites()
        
    }
    
    func loadFavorites() {
        Utils.startProgressAnimation()

        tableView.isHidden = true
        FavoritesService.getFavorites { (favorites) in
            self.favorites = favorites
            self.tableView.isHidden = false
            self.tableView.reloadData()
            
            Utils.stopProgressAnimation()
            
            if let favorites = favorites, favorites.count > 0 {
                self.secondView.isHidden = true
                self.firstView.isHidden = false
            } else {
                self.secondView.isHidden = false
                self.firstView.isHidden = true
            }
            print("Task 1 finished")
        } completionError: { (error) in
            print("error fetching favorites")
            Utils.stopProgressAnimation()
        }
    }
    
}

extension FavoritesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let del = self.favorites {
            return del.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BenefitTableViewCell.benefitId) as? BenefitTableViewCell else {
            fatalError()
        }
        cell.favDelegate = self
        cell.configure(with: self.favorites?[indexPath.row])
        return cell
    }
        
}

extension FavoritesViewController: FavoritesDelegate {
    func favoritesChanged() {
        loadFavorites()
    }
}
