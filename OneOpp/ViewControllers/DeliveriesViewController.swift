//
//  DeliveriesViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 10/07/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import UIKit
import CoreLocation

class DeliveriesViewController: ContentViewController {

    @IBOutlet var mainStack: UIStackView!
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    private var deliveries: [Delivery]?
    
    private var shouldShowLoadingCell = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        `tableView`.register(UINib(nibName: "DeliveryTableViewCell", bundle: nil), forCellReuseIdentifier: DeliveryTableViewCell.deliveryCellId)
    }
    
    override func appMovedToForegroundChild() {
        self.loadDeliveries()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tapGestureRecognizer?.isEnabled = false
        print("didAppear")
        secondView.isHidden = false
        firstView.isHidden = true
        self.loadDeliveries()
        
    }
    
    func loadDeliveries() {
        Utils.startProgressAnimation()

        tableView.isHidden = true
      
        DeliveryService.getDeliveries { (deliveries) in
            self.deliveries = deliveries
            self.tableView.isHidden = false
            self.tableView.reloadData()
            
            Utils.stopProgressAnimation()
            
            if let deliveries = deliveries, deliveries.count > 0 {
                self.secondView.isHidden = true
                self.firstView.isHidden = false
            } else {
                self.secondView.isHidden = false
                self.firstView.isHidden = true
            }
            print("Task 1 finished")
        } completionError: { (error) in
            print("error fetching deliveries")
            Utils.stopProgressAnimation()
        }
    }
    
}

extension DeliveriesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let del = self.deliveries {
            return del.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DeliveryTableViewCell.deliveryCellId) as? DeliveryTableViewCell else {
            fatalError()
        }
        cell.configure(with: self.deliveries?[indexPath.row])
        return cell
    }
        
}
