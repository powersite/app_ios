//
//  YouHaveACouponViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 06/07/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import AudioToolbox

class YouHaveACouponViewController: UIViewController {

    @IBOutlet var oneHand: UIImageView!
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    @IBOutlet var messageLbl: UILabel!
    @IBOutlet var deliveryButton: RedButton!
    
    var benefit: Benefit!
    
    public var errorMessage: String?
    
    var couponTakenSucc: Bool!
    var delivery = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if couponTakenSucc {
            self.deliveryButton.isEnabled = self.delivery
            self.firstView.isHidden = false
            self.secondView.isHidden = true
            self.oneHand.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 2.0, animations:{
                self.oneHand.transform = .identity
            }, completion: { _ in
                AudioServicesPlaySystemSound(1002);
            })
        } else {
            if let _ = self.errorMessage {
                self.messageLbl.text = self.errorMessage
            }
            self.firstView.isHidden = true
            self.secondView.isHidden = false
        }
    }
    
    @IBAction func takeAway(_ sender: Any) {
//       self.navigationController?.popViewController(animated: true)
        guard let nav = self.navigationController else {
            return
        }
        NavigationController.returnToMain(nav: nav, onCompletion: {
            Utils.showBannerSuccess(message: "El descuento se guardó en tus cupones, acercate al local y canjeá el código.")
        })
        
    }
    @IBAction func acceptWithError(_ sender: Any) {
        guard let nav = self.navigationController else {
            return
        }
        NavigationController.returnToMain(nav: nav)
    }
    
    @IBAction func delivery(_ sender: Any) {
        // call FORM
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let deliveryFormVC = storyboard.instantiateViewController(withIdentifier: "DeliveryForm") as? DeliveryFormViewController {
            deliveryFormVC.benefit = self.benefit
            self.navigationController?.pushViewController(deliveryFormVC, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
