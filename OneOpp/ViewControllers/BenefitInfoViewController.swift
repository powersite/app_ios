//
//  BenefitInfoViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 11/08/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class BenefitInfoViewController: UIViewController {
    var benefit: Benefit?
    
    @IBOutlet var benefitTitle: UILabel!
    @IBOutlet var benefitSubtitle: UILabel!
    
    @IBAction func acceptAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.benefitTitle.text = self.benefit?.business?.name
        self.benefitSubtitle.text = self.benefit?.business?.description
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
