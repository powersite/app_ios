//
//  MenuViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 31/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import SDWebImage

enum MenuAction {
    case home
    case account
    case share
    case howto
    case contact
    case commerce
    case tyc
    case policy
    case faq
}

protocol MenuViewControllerProtocol {
    func menuOptionSelected(_ option: MenuAction)
}

class MenuViewController: UIViewController {
    
    var delegate: MenuViewControllerProtocol?
    
    var currentActiveNav : UINavigationController?
    let placeholderImage = UIImage(named: "user-image")
    
    @IBOutlet var tableView: UITableView!
    
    let regularMenuCell = "MenuCellId"
    let separatorMenuCell = "SeparatorCellId"
    
    let menuItems = [MenuItem(image: "home", title: "Home", action: .home),
                     MenuItem(image: "account", title: "Mi Cuenta", action: .account),
                     MenuItem(image: "share", title: "Compartir", action: .share),
                     MenuItem(image: "howto", title: "Cómo funciona?", action: .howto),
                     MenuItem(image: "contact", title: "Contáctenos", action: .contact),
                     MenuItem(image: "commerce", title: "Suma un comercio", action: .commerce),
                     MenuItem(image: "tyc", title: "Términos y condiciones", action: .tyc),
                     MenuItem(image: "policy", title: "Política de privacidad", action: .policy),
                     MenuItem(image: "faq", title: "Preguntas frecuentes", action: .faq)]

    @IBOutlet var userName: UILabel!
    @IBOutlet var userAvatar: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let user = UserController.shared.user {
            self.userName.text = user.name
            if let userImage = user.userImage, !userImage.isEmpty {
//                let imgUrl = "http:\(itemImage)"
//                userAvatar?.sd_setShowActivityIndicatorView(true)
//                userAvatar?.sd_setIndicatorStyle(.gray)
                userAvatar?.sd_setImage(with: URL(string: user.userImage!), placeholderImage:placeholderImage)
            }
        }
        
        self.tableView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        if indexPath.row == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: separatorMenuCell)!
        } else {
            if let regularCell = tableView.dequeueReusableCell(withIdentifier: regularMenuCell) as? MenuTableViewCell {
                let arrIx = (indexPath.row < 2) ? indexPath.row : indexPath.row - 1
                regularCell.menuImage.image = UIImage(named: "menu-\(menuItems[arrIx].image)")
                regularCell.menuTitle.text = menuItems[arrIx].title
                cell = regularCell
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 { // separator
            return
        } else {
            let arrIx = (indexPath.row < 2) ? indexPath.row : indexPath.row - 1
            let menuItem = menuItems[arrIx]
            
            switch menuItem.action {
            case .home:
                if let mainVC = self.parent as? MainScreenViewController {
                    mainVC.hideSideMenu()
                }
            case .account:
                if let currentActiveNav = self.currentActiveNav,
                    let mainVC = self.parent as? MainScreenViewController {
                    mainVC.hideSideMenu()
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let profileVC = storyboard.instantiateViewController(withIdentifier: "MyAccount") as? MyAccountViewController {
                        currentActiveNav.pushViewController(profileVC, animated: true)
                    }
                }
            case .share:
                if let mainVC = self.parent as? MainScreenViewController {
                    mainVC.hideSideMenu()
                    mainVC.shareApp()
                }
            case .howto:
                if let currentActiveNav = self.currentActiveNav,
                    let mainVC = self.parent as? MainScreenViewController {
                    mainVC.hideSideMenu()
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let howtoVC = storyboard.instantiateViewController(withIdentifier: "HowTo") as? HowItWorksViewController {
                        currentActiveNav.pushViewController(howtoVC, animated: true)
                    }
                }
            case .contact:
                if let currentActiveNav = self.currentActiveNav,
                let mainVC = self.parent as? MainScreenViewController {
                mainVC.hideSideMenu()
                    NavigationController.showContact(from: currentActiveNav)
                }
            case .commerce:
                if let mainVC = self.parent as? MainScreenViewController {
                    mainVC.hideSideMenu()
                    NavigationController.navigateToAddCommerce()
                }
            case .tyc:
                if let currentActiveNav = self.currentActiveNav,
                    let mainVC = self.parent as? MainScreenViewController {
                    mainVC.hideSideMenu()
                    NavigationController.showDocument(from: currentActiveNav, for: .tyc)
                }
            case .policy:
                if let currentActiveNav = self.currentActiveNav,
                    let mainVC = self.parent as? MainScreenViewController {
                    mainVC.hideSideMenu()
                    NavigationController.showDocument(from: currentActiveNav, for: .policy)
                }
            
            case .faq:
                if let currentActiveNav = self.currentActiveNav,
                let mainVC = self.parent as? MainScreenViewController {
                mainVC.hideSideMenu()
                    NavigationController.showFAQ(from: currentActiveNav)
                }
            }
        }
    }
    
}
