//
//  SearchViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 11/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import CoreLocation

class SearchViewController: UIViewController {
    
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    @IBOutlet var searchTableView: UITableView!
    @IBOutlet var searchTerm: UITextField!
    @IBOutlet var loupeImage: UIImageView!
    @IBOutlet var loupeText: UILabel!
    @IBOutlet var loupeSubtext: UILabel!
    @IBOutlet var borderedView: UIView!
    //    static let benefitId = "BenefitCell"
    
    var coreLocationService: CLLocationManager?
    var location: CLLocation?
    
    @IBAction func searchTapped(_ sender: Any) {
        print("search")
        print(self.searchTerm.text ?? "")
//        TextfieldValidator.checkEmpty(searchTerm) ||
        if TextfieldValidator.checkMinLength(searchTerm, 3, errorMsg: "Debe ingresar al menos 3 caracteres") {
            loadBenefits(self.searchTerm.text)
        }
        
    }
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    private var benefitsController: BenefitsController!
    
    private var shouldShowLoadingCell = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configBorderedViews()
        
        //        indicatorView.color = .green
        
//        searchTableView.register(BenefitTableViewCell.self, forCellReuseIdentifier: SearchViewController.benefitId)
        searchTableView.register(UINib(nibName: "BenefitTableViewCell", bundle: nil), forCellReuseIdentifier: BenefitTableViewCell.benefitId)
//        searchTableView.prefetchDataSource = self as? UITableViewDataSourcePrefetching
        
//        loadBenefits()
        
        self.searchTableView.allowsSelection = true
        
        configureCoreLocation()
    }
    
    func configureCoreLocation() {
        coreLocationService = CLLocationManager()
        coreLocationService!.delegate = self
        coreLocationService!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//        coreLocationService!.requestWhenInUseAuthorization()
        coreLocationService!.requestLocation()
    }
    
    func configBorderedViews() {
        borderedView.borderColor = .black
        borderedView.borderWidth = 1.0
        borderedView.cornerRadius = 20
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        searchTerm.setError("", show: false)
    }
    
    func loadBenefits(_ term: String?) {
//        indicatorView.startAnimating()
        guard let location = self.location else {
            return
        }
        
        Utils.startProgressAnimation()
        
        searchTableView.isHidden = true
        //        tableView.separatorColor = .green
        //        tableView.dataSource = self
        
        let getBenefitServiceRequest = GetBenefitServiceRequest()
        getBenefitServiceRequest.page = 0
        getBenefitServiceRequest.size = 10
        getBenefitServiceRequest.orderBy = .DATE
        getBenefitServiceRequest.search_term = term
        getBenefitServiceRequest.latitud = "\(location.coordinate.latitude)"
        getBenefitServiceRequest.longitud = "\(location.coordinate.longitude)"
        benefitsController = BenefitsController(request: getBenefitServiceRequest, delegate: self)
        
        benefitsController.fetchBenefits(term)
        
        searchTableView.dataSource = self
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        benefitsController.totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BenefitTableViewCell.benefitId, for: indexPath) as! BenefitTableViewCell
        // 2
        if isLoadingCell(for: indexPath) {
            cell.configure(with: .none)
        } else {
            cell.configure(with: benefitsController.benefit(at: indexPath.row))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let benefit = benefitsController.benefit(at: indexPath.row)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let benefitDetailVC = storyboard.instantiateViewController(withIdentifier: "BenefitDetail") as? BenefitDetailViewController {
            benefitDetailVC.benefit = benefit
//            NavigationController.navigateFromTabBarVC(self, vc: benefitDetailVC)
//            self.pushViewController(benefitDetailVC, animated: true)
            self.navigationController?.pushViewController(benefitDetailVC, animated: true)
        }
    }
}

extension SearchViewController: BenefitsControllerDelegate {
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?) {
        
        if benefitsController.totalCount > 0 {
//            self.loupeImage.image = UIImage(named: "loupe-icon")
//            self.loupeText.text = "Busca en OneOpp"
//            self.loupeSubtext.text = "Los beneficios que más te interesan"
            secondView.isHidden = true
            firstView.isHidden = false
        } else {
            self.loupeImage.image = UIImage(named: "loupe-no-results")
            self.loupeText.text = "No hay beneficios"
            self.loupeSubtext.text = ""
            secondView.isHidden = false
            firstView.isHidden = true
        }
        // 1
        guard let newIndexPathsToReload = newIndexPathsToReload else {
//            indicatorView.stopAnimating()
            Utils.stopProgressAnimation()
            searchTableView.isHidden = false
            searchTableView.reloadData()
            return
        }
        // 2
        let indexPathsToReload = visibleIndexPathsToReload(intersecting: newIndexPathsToReload)
        searchTableView.reloadRows(at: indexPathsToReload, with: .automatic)
    }
    
    func onFetchFailed(with reason: String) {
//        indicatorView.stopAnimating()
        Utils.stopProgressAnimation()
        print("Warning. Reason: \(reason)")
    }
    
    func askUserToAllowLocation() {
        // Hide table
        secondView.isHidden = false
        firstView.isHidden = true
        
        self.showAlert(title: "Se necesita autorización de ubicación", msg: "Por favor, autorice la ubicación desde las opciones de configuración del dispositivo.")
    }
}

private extension SearchViewController {
  func isLoadingCell(for indexPath: IndexPath) -> Bool {
    return indexPath.row >= benefitsController.currentCount
  }
  
  func visibleIndexPathsToReload(intersecting indexPaths: [IndexPath]) -> [IndexPath] {
    let indexPathsForVisibleRows = searchTableView.indexPathsForVisibleRows ?? []
    let indexPathsIntersection = Set(indexPathsForVisibleRows).intersection(indexPaths)
    return Array(indexPathsIntersection)
  }
}

extension SearchViewController: CLLocationManagerDelegate {
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//       
//       requestUpdateLocationIfLocationEnabled()
//    }
    
    /*func requestUpdateLocationIfLocationEnabled() {
        guard CLLocationManager.locationServicesEnabled() else {
            print("CL disabled")
            self.askUserToAllowLocation()
            return
        }

        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse, .authorizedAlways:
            Utils.startProgressAnimation()
            self.coreLocationService!.requestLocation()
//            self.coreLocationService?.startUpdatingLocation()
//            self.location = self.coreLocationService?.location
//            self.loadBenefits()
        case .denied, .restricted, .notDetermined:
            print("CL NOT authorized")
            self.askUserToAllowLocation()
        }
    }*/
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.location = location
//            self.loadBenefits()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("CL location manager didFailWithError")
        Utils.stopProgressAnimation()
    }
}
