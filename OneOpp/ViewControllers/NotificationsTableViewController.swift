//
//  NotificationsTableViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 11/08/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Argo

class NotificationsTableViewController: ContentViewController, NotificationsControllerDelegate {
    @IBOutlet var mainStack: UIStackView!
    
    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationsController.shared.delegate = self
    }
    
    override func appMovedToForegroundChild() {
        print("Notifications: appMovedToForeground")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let notif = NotificationsController.shared.notifications, notif.count > 0 {
            self.tableView.reloadData()
            self.mainStack.arrangedSubviews.first?.isHidden = false
            self.mainStack.arrangedSubviews.last?.isHidden = true
        } else {
            self.mainStack.arrangedSubviews.first?.isHidden = true
            self.mainStack.arrangedSubviews.last?.isHidden = false
        }
    }
    
    func onNewNotifications() {
        self.tableView.reloadData()
    }
}

extension NotificationsTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotificationsController.shared.notifications?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationTableViewCell else {
            fatalError("Should get notif cell")
        }
        if let notif = NotificationsController.shared.notifications?[indexPath.row] {
            cell.configureWithNotification(notif)
        }
        return cell
    }
}
