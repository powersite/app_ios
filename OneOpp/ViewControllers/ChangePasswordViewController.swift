//
//  ChangePasswordViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 07/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet var borderedViews: [UIView]!
    @IBOutlet var oldPassword: UITextField!
    @IBOutlet var newPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureTextFields()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        oldPassword.setError("", show: false)
        newPassword.setError("", show: false)
    }
    
    func configureTextFields() {
        oldPassword.setPasswordEye()
        newPassword.setPasswordEye()
        for view in self.borderedViews {
            view.borderColor = .black
            view.borderWidth = 1.0
            view.cornerRadius = 20
        }
    }
    
    @IBAction func acceptAction(_ sender: Any) {
        var validPass1 = true
        if !TextfieldValidator.checkEmpty(oldPassword) || !TextfieldValidator.checkMinLength(oldPassword, 4, errorMsg: "El password es muy corto") {
            validPass1 = false
        }
        var validPass2 = true
        if !TextfieldValidator.checkEmpty(newPassword) || !TextfieldValidator.checkMinLength(newPassword, 4, errorMsg: "El password es muy corto") {
            validPass2 = false
        }
        
        if !(validPass1 && validPass2) { return }
        
        if TextfieldValidator.checkEqualPass(tf1: oldPassword, tf2: newPassword) {
            
            Utils.startProgressAnimation()
            ChangePasswordService.changePass(pass: newPassword.text!, completionSuccess: {
                Utils.stopProgressAnimation()
                self.performSegue(withIdentifier: "ShowPasswordChangeSuccess", sender: nil)
            }) { (error) in
                Utils.stopProgressAnimation()
                self.showAlert(title: error?.error, msg: error?.message)
            }
        }
            
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChangePasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text as NSString? {
            if textField.rightView?.tag == 998 {
                let updatedText = text.replacingCharacters(in: range, with: string)
                (textField.rightView as? UIButton)?.isEnabled = updatedText.count > 0
            }
        }
        return true
    }
}
