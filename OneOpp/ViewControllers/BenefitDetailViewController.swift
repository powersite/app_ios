//
//  BenefitDetailViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 28/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import SDWebImage
import MapKit

class BenefitDetailViewController: UIViewController {
    public var benefit: Benefit?

    @IBOutlet var benefitTitle: UILabel!
    @IBOutlet var benefitCategory: UILabel!
    @IBOutlet var benefitDescription: UILabel!
    @IBOutlet var benefitDelivery: UILabel!
    @IBOutlet var benefitImage: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var imgBenefitApp: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureBenefit()
        // Do any additional setup after loading the view.
    }
    
    func configureBenefit() {
        self.benefitTitle.text = benefit?.business?.name
        self.benefitCategory.text = benefit?.category?.name
        self.benefitDescription.text = benefit?.title
        if benefit?.delivery ?? true {
            self.benefitDelivery.superview?.backgroundColor = UIColor(red: 31/255, green: 201/255, blue: 91/255, alpha: 1)
            self.benefitDelivery.text = "Comercio con Delivery"
        } else {
            self.benefitDelivery.superview?.backgroundColor = UIColor(red: 242/255, green: 25/255, blue: 41/255, alpha: 1)
            self.benefitDelivery.text = "Comercio sin delivery"
        }
        if let imgName = benefit?.business?.thumbnail {
            let imgUrl = NetworkingController.Constants.IMAGES_PATH + imgName
            self.benefitImage.sd_setImage(with: URL(string: imgUrl)) { (image, _, _, _) in
            }
        }
        if let imgBenefitAppName = benefit?.imageBenefitApp {
            let imgUrl = NetworkingController.Constants.BENEFIT_IMAGE_PATH + imgBenefitAppName
            self.imgBenefitApp.sd_setImage(with: URL(string: imgUrl)) { (image, _, _, _) in
            }
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Utils.stopProgressAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func backButtonTouched(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func infoTouched(_ sender: Any) {
        print("info touched")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let benefitInfoVC = storyboard.instantiateViewController(withIdentifier: "BenefitInfo") as? BenefitInfoViewController {
            benefitInfoVC.benefit = self.benefit
            benefitInfoVC.providesPresentationContextTransitionStyle = true
            benefitInfoVC.definesPresentationContext = true
            benefitInfoVC.modalPresentationStyle = .overCurrentContext
            benefitInfoVC.modalTransitionStyle = .crossDissolve
            self.navigationController?.present(benefitInfoVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func carTouched(_ sender: Any) {
        print("car touched")
        guard let benefit = self.benefit, let lat = benefit.business?.latitud?.toDouble(), let long = benefit.business?.longitud?.toDouble() else {
            return
        }
        let place = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
        let destination = MKMapItem(placemark: place)
        destination.name = benefit.business?.name
        let items = [destination]
        let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        MKMapItem.openMaps(with: items, launchOptions: options)
    }
    
    @IBAction func shareTouched(_ sender: Any) {
        guard let benefit = self.benefit, let business = benefit.business, let busName = business.name, let disc = benefit.discount, let bTitle = benefit.title, let p = disc.label else {
            return
        }
        var share = Utils.Constants.BENEFIT_SHARE
        share = share.replacingOccurrences(of: "@business", with: busName)
        share = share.replacingOccurrences(of: "@discount", with: p)
        share = share.replacingOccurrences(of: "@description", with: bTitle)

        Utils.shareBenefit(textToShare: share, vc: self)
    }
    @IBAction func iWantItTouched(_ sender: Any) {
//        let sb = UIStoryboard(name: "Main", bundle: nil)
//        if let deliveryFormVC = sb.instantiateViewController(withIdentifier: "DeliveryForm") as? DeliveryFormViewController {
//            deliveryFormVC.benefit = self.benefit
//            self.navigationController?.pushViewController(deliveryFormVC, animated: true)
//        }
//        return
            
        /*
         // con el que estuve probando:
         self.performSegue(withIdentifier: DeliveryFormViewController.segueIdentifier, sender: self.benefit)
        return*/
        
        print("I want it")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let iWantItVC = storyboard.instantiateViewController(withIdentifier: "IWantItVC") as? IWantItViewController {
            iWantItVC.benefit = self.benefit
//            iWantItVC.modalPresentationStyle = .overFullScreen
            iWantItVC.delegate = self
            iWantItVC.providesPresentationContextTransitionStyle = true
            iWantItVC.definesPresentationContext = true
            iWantItVC.modalPresentationStyle = .overCurrentContext
            iWantItVC.modalTransitionStyle = .crossDissolve
            self.navigationController?.present(iWantItVC, animated: true, completion: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == DeliveryFormViewController.segueIdentifier {//"ShowDeliveryForm" {
            guard let vc = segue.destination as? DeliveryFormViewController else {
                fatalError()
            }
            vc.benefit = self.benefit
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BenefitDetailViewController: IWantACouponProtocol {
    func couponAccepted() {
        BenefitService.reserveBenefit(benefitId: self.benefit!.idBeneficio, completionSucessfully: { (coupon) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let oneCouponVC = storyboard.instantiateViewController(withIdentifier: "YouHaveOneCoupon") as? YouHaveACouponViewController {
                oneCouponVC.benefit = self.benefit
                oneCouponVC.couponTakenSucc = true
                oneCouponVC.delivery = self.benefit?.delivery ?? true
                self.navigationController?.pushViewController(oneCouponVC, animated: true)
            }
        }) { (error) in // Beneficio ya tomado
            print(error)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let oneCouponVC = storyboard.instantiateViewController(withIdentifier: "YouHaveOneCoupon") as? YouHaveACouponViewController {
                oneCouponVC.couponTakenSucc = false
                oneCouponVC.errorMessage = error
                self.navigationController?.pushViewController(oneCouponVC, animated: true)
            }
        }
        
    }
}

extension BenefitDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
//            case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DiscountBenefitCell") as? BenefitDetailTableViewCell else {
                fatalError("revisar")
            }
            cell.benefitDesc!.text = benefit?.discount?.label
            if let colorHexString = benefit?.discount?.color, let colorDisc = UIColor(hexString: "#\(colorHexString)") {
                cell.discountView?.backgroundColor = colorDisc
            }
            return cell
        } else if indexPath.row == 5 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentsBenefitCell") as? BenefitDetailTableViewCell else {
                fatalError("revisar")
            }
            benefit?.business?.cards?.forEach({ (card) in
                if let cardImg = UIImage(named: "\(card.lowercased())-icon") {
                    let imageView = UIImageView(image: cardImg)
                    NSLayoutConstraint.activate([
                        imageView.widthAnchor.constraint(equalToConstant: 30.0),
                        imageView.heightAnchor.constraint(equalToConstant: 30.0)
                    ])
                    cell.cardsStack!.addArrangedSubview(imageView)
                }
            })
            return cell
        } else if indexPath.row == 3 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ClockBenefitCell") as? BenefitDetailTableViewCell else {
                fatalError("revisar")
            } // TODO: read from api response (not implemented yet)
            cell.benefitImage.image = UIImage(named: "sand-clock")
//            cell.startRotatingImage()
//            cell.benefitDesc?.text = benefit?.dates.endDate?.offsetFrom(date: Date())
//            cell.endDate = benefit?.dates.endDate
            if let endDate = benefit?.dates.endDate {
                cell.startClicking(endDate: endDate)
            }
            
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RegularBenefitCell") as? BenefitDetailTableViewCell else {
                fatalError("revisar")
            }
            switch indexPath.row {
            case 1:
                cell.benefitImage.image = UIImage(named: "calendar-icon")
                cell.benefitDesc!.text = benefit?.dates.startDate?.toString()
            case 2:
                cell.benefitImage.image = UIImage(named: "calendar-icon")
                cell.benefitDesc!.text = benefit?.dates.endDate?.toString()
            case 4:
                cell.benefitImage.image = UIImage(named: "location-icon")
                cell.benefitDesc!.text = benefit?.business?.address
            case 6:
                cell.benefitImage.image = UIImage(named: "file-icon")
                cell.benefitDesc!.text = benefit?.description
            default:
                break
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print("didEndDisplaying")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 {
            return 70
        } else {
            return 44
        }
    }
    
    
}
