//
//  CouponActivitySelectorViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 04/07/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class CouponActivitySelectorViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    @IBOutlet var segmented: UISegmentedControl!
    @IBAction func segmentedValueChanged(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        self.dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "popoverSegue" {
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController!.delegate = self
        }
    }

//    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
//        setAlphaOfBackgroundViews(alpha: 1)
//    }
//
//    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
//        setAlphaOfBackgroundViews(alpha: 0.7)
//    }
//
//    func setAlphaOfBackgroundViews(alpha: CGFloat) {
//        let statusBarWindow = UIApplication.shared.value(forKey: "statusBarWindow") as? UIWindow
//        UIView.animate(withDuration: 0.2) {
//            statusBarWindow?.alpha = alpha;
//            self.view.alpha = alpha;
//            self.navigationController?.navigationBar.alpha = alpha;
//        }
//    }
}
