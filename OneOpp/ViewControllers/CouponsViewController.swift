//
//  CouponsViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 15/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class CouponsViewController: ContentViewController, UICollectionViewDelegateFlowLayout {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var mainStack: UIStackView!
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    @IBOutlet var benefitsHand: UIImageView!
    
    var selectedStatus: CouponService.Status = .ACTIVE
    
    var coupons: [Coupon]?
    let cellId = "CouponCell"
    
    var exchangeCouponTopShapeImg: UIImage?
    var exchangeCouponBottomShapeImg: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageControl.currentPageIndicatorTintColor = .orange
        
        print("Coupon viewDidLoad")
        
        self.exchangeCouponTopShapeImg = UIImage(named: "coupon-button-separator")
        self.exchangeCouponBottomShapeImg = UIImage(named: "coupon-bottom")
        self.collectionView.addGestureRecognizer(panGestureRecognizer!)
    }
    
    override func appMovedToForegroundChild() {
        loadCoupons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("Coupon viewDidAppear")
        self.addCouponTypeButton()
        
        panGestureRecognizer?.isEnabled = false
        loadCoupons()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.benefitsHand.stopAnimating()
    }
    
    func  setupPagesControl() {
        self.pageControl.numberOfPages = self.coupons?.count ?? 0
    }

    func loadCoupons () {
        fetchCouponsWithStatus()//.ACTIVE)
    }
    
    func fetchCouponsWithStatus() {//_ status: CouponService.Status) {
        Utils.startProgressAnimation()
        CouponService.getCoupons(status: selectedStatus, completionSucessfully: { (coupons) in
            Utils.stopProgressAnimation()
            self.coupons = coupons
            
            if (coupons?.count ?? 0) > 0 {
                self.firstView.isHidden = false
                self.firstView.frame.size = self.mainStack.bounds.size
                self.secondView.isHidden = true
                
                self.collectionView.reloadData()
                self.setupPagesControl()
                
                self.benefitsHand.stopAnimating()
            } else {
                self.animateHand()
                self.firstView.isHidden = true
                self.secondView.isHidden = false
                self.secondView.frame.size = self.mainStack.bounds.size
            }
            
        }, completionError: { (err) in
            Utils.stopProgressAnimation()
            print(err.debugDescription)
        })
    }
    
    func animateHand() {
        self.benefitsHand.frame = CGRect(origin: CGPoint(x: self.benefitsHand.frame.origin.x, y: self.benefitsHand.frame.origin.y - 30), size: self.benefitsHand.frame.size)
        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse] , animations: {
            self.benefitsHand.frame = CGRect(origin: CGPoint(x: self.benefitsHand.frame.origin.x, y: self.benefitsHand.frame.origin.y + 30), size: self.benefitsHand.frame.size)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = collectionView.bounds.size
        return cellSize
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentIndex = self.collectionView.contentOffset.x / self.collectionView.frame.size.width
        self.pageControl.currentPage = Int(currentIndex)
    }
    
    override func couponStatusSelected(_ status: CouponService.Status) {
        print("reload coupons: \(status)")
        selectedStatus = status
        fetchCouponsWithStatus()//status)
    }

}

extension CouponsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.coupons?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? CouponCollectionViewCell else {
            fatalError("review")
        }
        
        if let coupon = self.coupons?[indexPath.row] {
            cell.configureCoupon(coupon, status: selectedStatus)
            cell.delegate = self
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
}

extension CouponsViewController: CouponCellProtocol {
    
    func exchangeCoupon(id: Int) {
        print("exchange: \(String(describing: id))")
    }
    
    func shareCoupon(textToShare: String) {
        Utils.shareBenefit(textToShare: textToShare, vc: self)
    }
    
    func deleteCoupon(id: Int) {
        let alertController = UIAlertController(title: nil, message: "¿Eliminar cupón?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (action) in
            print("cancel")
        }
        let oKAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            print("delete: \(String(describing: id))")
            CouponService.deleteCoupon(couponId: id, completionSucessfully: { () in
                print("success")
                self.loadCoupons()
            }, completionError: { (err) in
                print(err.debugDescription)
            })
        }
        alertController.addAction(oKAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
}
