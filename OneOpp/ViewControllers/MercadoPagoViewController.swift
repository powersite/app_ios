//
//  MercadoPagoViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 19/04/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import UIKit

class MercadoPagoViewController: UIViewController {
    
    weak var delegate: MercadoPagoProtocol?
    
    @IBAction func acceptAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.mercadoPagoAccepted()
        }
    }
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.mercadoPagoCanceled()
        }
    }
}

protocol MercadoPagoProtocol: NSObject {
    func mercadoPagoCanceled()
    func mercadoPagoAccepted()
}
