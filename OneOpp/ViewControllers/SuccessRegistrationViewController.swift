//
//  SuccessRegistrationViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class SuccessRegistrationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func aceptar(_ sender: Any) {
        var loginVc: UIViewController?
        for vc in self.navigationController!.viewControllers {
            if vc.isKind(of: LoginViewController.self) {
                loginVc = vc
                break
            }
        }
        if let popVC = loginVc {
            self.navigationController?.popToViewController(popVC, animated: true)
        }
        
        print("aceptar")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
