//
//  TabBarViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 31/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        navigationController?.setNavigationBarHidden(false, animated: animated)
        guard let couponTabBarItem = self.tabBar.items?[1] else {
            fatalError("Second tab bar item is for Coupons")
        }
        
        CouponService.getCount(completionSucessfully: { (count) in
            if let count = count, count > 0 {
                couponTabBarItem.badgeValue = "●"
                couponTabBarItem.badgeColor = .clear
                couponTabBarItem.setBadgeTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], for: .normal)
            }
        }, completionError: { (_) in
            print("Error on getting Coupons count")
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
