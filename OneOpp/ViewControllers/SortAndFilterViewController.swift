//
//  SortAndFillterViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 27/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class SortAndFilterViewController: UIViewController {
    
    weak var delegate: SortAndFilterProtocol?
    
    enum SortType: Int {
        case latest = 0
        case best
        case nearest
    }
    
    var categories: [Category]?
    
//    static let categories = [("Todos", ""),
//                        ("Bebidas", "bebidas"),
//                        ("Indumentaria", "indumentaria"),
//                        ("Heladería", "heladeria"),
//                        ("Gastronomía", "gastronomia"),
//                        ("Calzado", "calzado"),
//                        ("Tecnología", "tecnologia"),
//                        ("Librería", "libreria"),
//                        ("Juegos y Juguetes", "juegos y juguetes"),
//                        ("Gimnasio & Deportes", "gimnasio y deportes"),
//                        ("Electrodomésticos", "electrodomesticos"),
//                        ("Farmacia", "farmacia"),
//                        ("Computación", "computacion"),
//                        ("Animales y Mascotas", "animales y mascotas"),
//                        ("Hogar, Muebles y Jardín", "hogar, muebles y jardin"),
//                        ("Música", "musica"),
//                        ("Joyería & Relojes", "joyeria y relojes"),
//                        ("Cotillón", "cotillon"),
//                        ("Servicios", "servicios"),
//                        ("Viajes & Turismo", "viajes y turismo"),
//                        ("Tatoo & Piercing", "tatoo & piercing"),
//                        ("Blanquería", "blanqueria"),
//                        ("Art. Limpieza", "art limpieza"),
//                        ("Bazar", "bazar")
//    ]
    
    var selectedCategory: Category? {
        didSet {
            self.itemsButton.setTitle(selectedCategory?.name, for: .normal)
        }
    }
    
    var sortSelected: SortType? = .nearest
    
    @IBOutlet var sortButtons: [SortYellowButton]!
    
    @IBOutlet var itemsButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        CategoriesService.getCategories(completionSucessfully: { (categories) in
            self.categories = categories
            self.categories?.sort(by: { (firstCat, secCat) -> Bool in
                firstCat.position! < secCat.position!
            })
            self.categories?.insert(Category(id: -1, name: "Todos", position: -1), at: 0)
            
            self.selectedCategory = self.categories?.first
            
        }, completionError: { (err) in
            print(err.debugDescription)
        })
    }
    
    @IBAction func sortOptionButtonTapped(_ sender: UIButton) {
        for btn in self.sortButtons {
            if btn ==  sender {
                btn.isSelected = true
                
                sortSelected = SortType(rawValue: btn.tag)
            } else {
                btn.isSelected = false
            }
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func resetTapped(_ sender: Any) {
        print("reset")
        self.dismiss(animated: true, completion: {
            if let d = self.delegate {
                d.resetSortAndFilter()
            }
        })
    }
    @IBAction func changeItem(_ sender: Any) {
        let itemPicker = UIPickerView()
        itemPicker.reloadAllComponents()
        itemPicker.delegate = self
        itemPicker.dataSource = self
        
        let loadOptions = UIAlertController(title: "Seleccione una opción para avanzar", message: nil, preferredStyle: .actionSheet)
        
        guard let categories = categories else {
            return
        }
        for cat in categories {
            let itemAction = UIAlertAction(title: cat.name, style: .default) { (action) in
                self.selectedCategory = cat
            }
            loadOptions.addAction(itemAction)
            
        }
        
        self.present(loadOptions, animated: true, completion: nil)
    }
    
    @IBAction func accept(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            print("Sort: \(String(describing: self.sortSelected!))")
            print("Rubro: \(String(describing: self.selectedCategory?.name))")
            
            if let d = self.delegate, let sort = self.sortSelected, let cat = self.selectedCategory {
                d.sortAndItemChosen(sort, cat)
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SortAndFilterViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categories?.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return self.categories?[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedCategory = self.categories?[row]
    }
}

protocol SortAndFilterProtocol: NSObject {
    func sortAndItemChosen(_ sort: SortAndFilterViewController.SortType, _ cat: Category)
    func resetSortAndFilter()
}
