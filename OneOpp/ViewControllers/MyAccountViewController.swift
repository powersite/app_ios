//
//  MyAccountViewController.swift
//  
//
//  Created by Vanina Geneiro on 05/05/2020.
//

import UIKit
import Foundation

class MyAccountViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var userEmail: UILabel!
    
    let accountOptions = [
        [
            "key": "pass",
            "title": "Cambiar password"
        ], [
            "key": "notif",
            "title": "Notificaciones"
        ], [
            "key": "close_session",
            "title": "Cerrar sesión"
        ]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userName.text = UserController.shared.user?.name
        self.userEmail.text = UserController.shared.user?.email
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MyAccountViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleCell") else {
            fatalError("Cell should not be nil")
        }
        cell.textLabel?.text = accountOptions[indexPath.row]["title"]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let item = accountOptions[indexPath.row]["key"] else {
            fatalError("Debería tener key")
        }
        
        print(item)
        switch item {
        case "pass":
            print("pass")
            performSegue(withIdentifier: "ChangePass", sender: nil)
        case "notif":
            print("notif")
            performSegue(withIdentifier: "ShowNotif", sender: nil)
        case "close_session":
            closeSession()
        default:
            return
        }
        
    }
    
    func closeSession() {
        let alertController = UIAlertController(title: nil, message: "¿Está seguro que quiere cerrar la sesión?", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (action) in
            print("cancel")
        }
        let oKAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            UserController.shared.closeSession()
            NavigationController.shared.showLogInScreen()
        }
        alertController.addAction(oKAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true)
    }
    
}
