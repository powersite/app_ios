//
//  LoginViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 23/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import GoogleSignIn
import FirebaseAuth
import AuthenticationServices
import CryptoKit

class LoginViewController: UIViewController {
    @IBOutlet var borderedViews: [UIView]!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var bckgColorViews: UIView!
    @IBOutlet var loginProviderStackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordField.setPasswordEye()
        
        setupProviderLoginView()

        // Do any additional setup after loading the view.
        self.bckgColorViews.addGradientBackground("#E72454", "#FAB53E")        
        for view in self.borderedViews {
            view.borderColor = .black
            view.borderWidth = 1.0
            view.cornerRadius = 20
        }
    }
    
    func setupProviderLoginView() {
        // Google Sign in
        let googleAuthButton = GIDSignInButton()
        googleAuthButton.style = .wide
        self.loginProviderStackView.addArrangedSubview(googleAuthButton)
        
        // Apple Sign in
        let appleAuthButton = ASAuthorizationAppleIDButton()
        appleAuthButton.addTarget(self, action: #selector(handleAuthorizationAppleIDButtonPress), for: .touchUpInside)
        self.loginProviderStackView.addArrangedSubview(appleAuthButton)
    }
        
    /// - Tag: perform_appleid_request
    @objc
    func handleAuthorizationAppleIDButtonPress() {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // Unhashed nonce.
    fileprivate var currentNonce: String?

    @available(iOS 13, *)
    private func sha256(_ input: String) -> String {
      let inputData = Data(input.utf8)
      let hashedData = SHA256.hash(data: inputData)
      let hashString = hashedData.compactMap {
        return String(format: "%02x", $0)
      }.joined()

      return hashString
    }
    
    @IBAction func login(_ sender: Any) {
        var validEmail = true
        if !TextfieldValidator.checkEmpty(emailField) || !TextfieldValidator.isValidEmail(field: emailField){
            validEmail = false
        }
        
        var validPass = true
        if !TextfieldValidator.checkEmpty(passwordField) {
            validPass = false
        }
        
        if !(validEmail && validPass) { return }
        
        let email = emailField.text!
        let pass = passwordField.text!
        
        Utils.startProgressAnimation()
        UserController.shared.logIn(email: email, password: pass, completionSuccess: {
            Utils.stopProgressAnimation()
            self.performSegue(withIdentifier: "showMainScreen", sender: nil)
        }) { (error) in
            print("error")
            Utils.stopProgressAnimation()
            self.showAlert(title: error?.error, msg: error?.message)
        }
    }
    
    private func randomNonceString(length: Int = 32) -> String {
      precondition(length > 0)
      let charset: Array<Character> =
          Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
      var result = ""
      var remainingLength = length

      while remainingLength > 0 {
        let randoms: [UInt8] = (0 ..< 16).map { _ in
          var random: UInt8 = 0
          let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
          if errorCode != errSecSuccess {
            fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
          }
          return random
        }

        randoms.forEach { random in
          if remainingLength == 0 {
            return
          }

          if random < charset.count {
            result.append(charset[Int(random)])
            remainingLength -= 1
          }
        }
      }

      return result
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == passwordField){
            if let text = textField.text as NSString? {
                if textField.rightView?.tag == 998 {
                    let updatedText = text.replacingCharacters(in: range, with: string)
                    (textField.rightView as? UIButton)?.isEnabled = updatedText.count > 0
                }
            }
        }
        return true
    }
}

extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let _ = error { return }
        Utils.startProgressAnimation()
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                          accessToken: authentication.accessToken)
        UserController.shared.firLogin(credential: credential) {
            Utils.stopProgressAnimation()
            self.performSegue(withIdentifier: "showMainScreen", sender: nil)
        } completionError: {
            Utils.stopProgressAnimation()
        }
    }
}

@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerDelegate {
    /// - Tag: did_complete_authorization
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
            fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
            print("Unable to fetch identity token")
            return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
            print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
            return
            }
            // Initialize a Firebase credential.
            Utils.startProgressAnimation()
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                    idToken: idTokenString,
                                                    rawNonce: nonce)
            
            UserController.shared.firLogin(credential: credential) {
                Utils.stopProgressAnimation()
                self.performSegue(withIdentifier: "showMainScreen", sender: nil)
            } completionError: {
                Utils.stopProgressAnimation()
            }
        }
      }

      func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        print("Sign in with Apple errored: \(error)")
      }
    
    private func showPasswordCredentialAlert(username: String, password: String) {
        let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
        let alertController = UIAlertController(title: "Keychain Credential Received",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension LoginViewController: ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
