//
//  OnboardingCollectionViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 18/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class OnboardingCollectionViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var nextButtonWidth: NSLayoutConstraint!
    
    var onboardingItems: [Dictionary<String, Any>]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let jsonItems = Utils.readJSONFromFile(fileName: "onboarding"), let items = jsonItems as? Dictionary<String, Any>, let screens = items["screens"] as? [Dictionary<String, Any>]{
            self.onboardingItems = screens
        }
        self.setupPagesControl()
        
        self.collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func nextPage(_ sender: Any) {
        if self.pageControl.currentPage < (self.onboardingItems?.count ?? 0) - 1 {
            self.pageControl.currentPage += 1
            selectItemInCollection()
            self.showproperButtonDesc(self.pageControl.currentPage)
        } else {
            self.performSegue(withIdentifier: "showLoginScreen", sender: nil)
        }
    }
    
    func selectItemInCollection() {
        let ip = IndexPath(row: self.pageControl.currentPage, section: 0)
        self.collectionView.selectItem(at: ip, animated: true, scrollPosition: .centeredHorizontally)
    }
    
    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
    }
    
    func  setupPagesControl() {
        self.pageControl.numberOfPages = self.onboardingItems?.count ?? 0
    }

}

extension OnboardingCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return onboardingItems?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var onboardingCell = UICollectionViewCell()
        if let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "OnboardingCell", for: indexPath) as? OnboardingCollectionViewCell {
            
            if let screen = self.onboardingItems?[indexPath.row] {
                if let image = screen["image"] as? String {
                    cell.cellImage.image = UIImage(named: image)
                }
                if let colors = screen["colors"] as? [String], colors.count == 2 {
                    
                    cell.backColorsView.addGradientBackground(colors.first!, colors.last!)
                }
                if let text = screen["text"] as? String {
                    cell.cellTitle.text = text
                }
            }
            onboardingCell = cell
        }
        
        return onboardingCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds.size
        return screenSize
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let current = Int(self.collectionView.contentOffset.x / self.collectionView.frame.width)
        self.pageControl.currentPage = current
        self.showproperButtonDesc(current)
    }
    
    func showproperButtonDesc(_ current: Int) {
        if current == onboardingItems!.endIndex - 1 {
            setLoginButton()
        } else {
            setNextButton()
        }
    }
    
    func setNextButton() {
        self.nextButton.setImage(UIImage(named: "nextArrow"), for: .normal)
        self.nextButton.setTitle("", for: .normal)
        self.nextButtonWidth.constant = 25
    }
    
    func setLoginButton() {
        self.nextButton.setImage(nil, for: .normal)
        self.nextButton.setTitle("LOGIN", for: .normal)
        self.nextButtonWidth.constant = 54
    }
}
