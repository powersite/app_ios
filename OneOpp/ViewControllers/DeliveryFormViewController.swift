//
//  DeliveryFormViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/10/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import MercadoPagoSDK

class DeliveryFormViewController: UIViewController {
    static let segueIdentifier = "ShowDeliveryForm"
    private let placeholder = "Observaciones"
    
    public var benefit: Benefit?
    
    @IBOutlet var borderedViews: [UIView]!
    @IBOutlet var nameAndLastName: UITextField!
    @IBOutlet var phone: UITextField!
    @IBOutlet var streetAddress: UITextField!
    @IBOutlet var numberAddress: UITextField!
    @IBOutlet var floor: UITextField!
    @IBOutlet var apartment: UITextField!
    @IBOutlet var betweenStreets: UITextField!
    @IBOutlet var comments: UITextView!
    @IBOutlet var acceptButton: RedButton!
    
    let DELIVERY_INFO = "DELIVERY_INFO"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configBorderedViews()
        self.navigationItem.title = "Delivery"
        self.comments.delegate = self
        configureTextViewAppearance()
        configureAcceptButton()
    }
    
    func configureTextViewAppearance() {
        comments.text = placeholder
        comments.textColor = .lightGray
        
        comments.selectedTextRange = comments.textRange(from: comments.beginningOfDocument, to: comments.beginningOfDocument)
    }
    
    func configureAcceptButton() {
        if let business = benefit?.business, business.hasMPenabled {
            acceptButton.setTitle("PAGAR", for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let deliveryInfo = UserController.shared.getSavedDeliveryInfo() {
            self.loadFieldsWithDeliveryInfo(delivery: deliveryInfo)
        }
    }
    
    func loadFieldsWithDeliveryInfo(delivery: Delivery) {
        print(delivery)
        
        if let _ = delivery.betweenStreet {
            self.betweenStreets.text = delivery.betweenStreet
        }
        if let _ = delivery.department {
            self.apartment.text = delivery.department
        }
        if let _ = delivery.extraData {
            self.comments.textColor = .black
            self.comments.text = delivery.extraData
        }
        if let floor = delivery.floor {
            self.floor.text = String(floor)
        }
        if let _ = delivery.name {
            self.nameAndLastName.text = delivery.name
        }
        if let _ = delivery.number {
            self.numberAddress.text = delivery.number
        }
        if let _ = delivery.phone {
            self.phone.text = delivery.phone
        }
        if let _ = delivery.street {
            self.streetAddress.text = delivery.street
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func configBorderedViews() {
        for view in self.borderedViews {
            view.borderColor = .black
            view.borderWidth = 1.0
            view.cornerRadius = 20
        }
    }
    
    @IBAction func acceptAction(_ sender: Any) {
        if let business = benefit?.business, business.hasMPenabled {
            openMercadoPagoPopup()
        }
    }
    
    func sendDelivery() {
        var validName = true
        if !TextfieldValidator.checkEmpty(nameAndLastName) || !TextfieldValidator.checkEmpty(phone) || !TextfieldValidator.checkEmpty(streetAddress) || !TextfieldValidator.checkEmpty(numberAddress) || !TextfieldValidator.checkEmpty(betweenStreets) {
            validName = false
        }
        
//        guard let msg = textView.text, !msg.isEmpty, textView.textColor != .lightGray else {
//            return
//        }
        var extraData: String?
        if let notes = comments.text, !notes.isEmpty, comments.textColor != .lightGray {
            extraData = notes
        }
        
        if validName {
            print("valid")
            // TODO: businessObservation??
            let delivery = Delivery(id: nil,
                                    benefitId: self.benefit?.idBeneficio,
                                    betweenStreet: self.betweenStreets.text,
                                    businessId: self.benefit?.business?.id,
                                    businessObservation: self.benefit?.business?.description,
                                    deliveryStatusId: nil,
                                    deliveryStatusName: nil,
                                    department: self.apartment.text,
                                    deviceToken: nil,
                                    email: UserController.shared.user?.email,
                                    extraData: extraData,
                                    floor: Int(self.floor.text ?? "0"),
                                    name: self.nameAndLastName.text,
                                    number: self.numberAddress.text,
                                    phone: self.phone.text,
                                    street: self.streetAddress.text)
            
            DeliveryService.postDelivery(delivery: delivery, completionSucessfully: {
                /*let alert = UIAlertController(title: "Datos guardados", message: "Su envío será entregado", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (_) in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)*/
                /*if let nav = self.navigationController {
                    NavigationController.returnToMain(nav: nav)
                }*/
            }) { (error
                ) in
                print(error.debugDescription)
            }
        } else {
            print("no valid")
        }
    }
    
    func openMercadoPagoPopup() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let mercadoPagoVC = storyboard.instantiateViewController(withIdentifier: "MercadoPagoVC") as? MercadoPagoViewController {
            mercadoPagoVC.delegate = self
            mercadoPagoVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(mercadoPagoVC, animated: false, completion: nil)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        Utils.showBannerSuccess(message: "Tenés un cupón asignado.")
    }
    
}

extension DeliveryFormViewController: MercadoPagoProtocol {
    func mercadoPagoCanceled() {
        print("Cancel protocol method called")
    }
    
    func mercadoPagoAccepted() {
        print("Accept protocol method called")
        guard let benefit = self.benefit, let business = benefit.business, let publicKey = business.mpPublicKey else {
            return
        }
        Utils.startProgressAnimation()
        MercadoPagoService.createPayment(benefit: benefit) { (preferenceId) in
            print("MP successful")
            Utils.stopProgressAnimation()
            MercadoPagoCheckout.init(builder: MercadoPagoCheckoutBuilder.init(publicKey: publicKey, preferenceId: preferenceId))
               .start(navigationController: self.navigationController!, lifeCycleProtocol: self)
        } completionError: { (error) in
            print("MP error")
            Utils.stopProgressAnimation()
        }
        
        self.sendDelivery()

    }
}

extension DeliveryFormViewController: PXLifeCycleProtocol {
    func finishCheckout() -> ((_ payment: PXResult?) -> Void)? {
        return  ({ (_ payment: PXResult?) in
            print(payment)
            //            self.navigationController?.popToRootViewController(animated: false)
            if let nav = self.navigationController {
                NavigationController.returnToMain(nav: nav)
            }
        })
    }
    
    func cancelCheckout() -> (() -> Void)? {
        return {
            //            self.navigationController?.popToRootViewController(animated: true)
            if let nav = self.navigationController {
                NavigationController.returnToMain(nav: nav)
            }
        }
    }
}

extension DeliveryFormViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == .lightGray {
            textView.text = nil
            textView.textColor = .black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholder
            textView.textColor = .lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
         // Combine the textView text and the replacement text to
           // create the updated text string
           let currentText:String = textView.text
           let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

           // If updated text view will be empty, add the placeholder
           // and set the cursor to the beginning of the text view
           if updatedText.isEmpty {

               textView.text = placeholder
               textView.textColor = .lightGray

               textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
           }

           // Else if the text view's placeholder is showing and the
           // length of the replacement string is greater than 0, set
           // the text color to black then set its text to the
           // replacement string
            else if textView.textColor == .lightGray && !text.isEmpty {
               textView.textColor = .black
               textView.text = text
           }

           // For every other case, the text should change with the usual
           // behavior...
           else {
               return true
           }

           // ...otherwise return false since the updates have already
           // been made
           return false
    }
}
