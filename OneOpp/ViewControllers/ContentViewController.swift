//
//  ContentViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 31/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Foundation

class ContentViewController: UIViewController {
    
  let menuButton = UIButton(type: .custom)
    var tapGestureRecognizer: UITapGestureRecognizer?
    var panGestureRecognizer: UIPanGestureRecognizer?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let notificationCenter = NotificationCenter.default
    notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)

    menuButton.setImage(UIImage(named: "menu-icon") , for: .normal)
    menuButton.contentMode = .scaleAspectFit
    menuButton.translatesAutoresizingMaskIntoConstraints = false
    
    menuButton.addTarget(self, action: #selector(menuButtonTapped(_:)), for: .touchUpInside)
    
    let barbutton = UIBarButtonItem(customView: menuButton)
    self.navigationItem.leftBarButtonItem = barbutton
    
    NSLayoutConstraint.activate([
        menuButton.widthAnchor.constraint(equalToConstant: 30.0),
        menuButton.heightAnchor.constraint(equalToConstant: 30.0)
    ])
    
    menuButton.clipsToBounds = true
    
    panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)) )
    tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)) )
    view.addGestureRecognizer(panGestureRecognizer!)
    view.addGestureRecognizer(tapGestureRecognizer!)
    
//    tapGestureRecognizer?.cancelsTouchesInView = false
  }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationItem.setRightBarButtonItems([], animated: false)
    }
    
    let infoButton = UIButton(type: .custom)
    let searchButton = UIButton(type: .custom)
    let sortAndFilterButton = UIButton(type: .custom)
    let couponTypeButton = UIButton(type: .custom)
    let favoritesButton = UIButton(type: .custom)
    
    func addInfoButton() {
        infoButton.setImage(UIImage(named: "white-info-icon") , for: .normal)
        infoButton.contentMode = .scaleAspectFit
        infoButton.translatesAutoresizingMaskIntoConstraints = false
        
        infoButton.addTarget(self, action: #selector(infoButtonTapped(_:)), for: .touchUpInside)
        
        let barbutton = UIBarButtonItem(customView: infoButton)
        self.navigationItem.rightBarButtonItems?.append(barbutton)
        
        NSLayoutConstraint.activate([
            infoButton.widthAnchor.constraint(equalToConstant: 25.0),
            infoButton.heightAnchor.constraint(equalToConstant: 25.0)
        ])
        
        // Make the profile button become circular
        infoButton.clipsToBounds = true
    }
    func addSortAndFilterButton() {
        sortAndFilterButton.setImage(UIImage(named: "sortAndFilter") , for: .normal)
        sortAndFilterButton.contentMode = .scaleAspectFit
        sortAndFilterButton.translatesAutoresizingMaskIntoConstraints = false
        
        // function performed when the button is tapped
        sortAndFilterButton.addTarget(self, action: #selector(sortAndFilterButtonTapped(_:)), for: .touchUpInside)
        
        // Add the profile button as the left bar button of the navigation bar
        let barbutton = UIBarButtonItem(customView: sortAndFilterButton)
        self.navigationItem.rightBarButtonItems?.append(barbutton)
//        self.navigationItem.rightBarButtonItem = barbutton
        
        // Set the width and height for the profile button
        NSLayoutConstraint.activate([
            sortAndFilterButton.widthAnchor.constraint(equalToConstant: 25.0),
            sortAndFilterButton.heightAnchor.constraint(equalToConstant: 25.0)
        ])
        
        // Make the profile button become circular
    //    profileButton.layer.cornerRadius = 35.0 / 2
        sortAndFilterButton.clipsToBounds = true
    }
    
    func addSearchButton() {
        searchButton.setImage(UIImage(named: "white-loupe") , for: .normal)
        searchButton.contentMode = .scaleAspectFit
        searchButton.translatesAutoresizingMaskIntoConstraints = false
        
        searchButton.addTarget(self, action: #selector(searchButtonTapped(_:)), for: .touchUpInside)
        
        let barbutton = UIBarButtonItem(customView: searchButton)
        self.navigationItem.rightBarButtonItem = barbutton
        
        NSLayoutConstraint.activate([
            searchButton.widthAnchor.constraint(equalToConstant: 25.0),
            searchButton.heightAnchor.constraint(equalToConstant: 25.0)
        ])
        
        searchButton.clipsToBounds = true
    }
    
    func addFavoritesButton() {
        favoritesButton.setImage(UIImage(named: "white-star-icon") , for: .normal)
        favoritesButton.contentMode = .scaleAspectFit
        favoritesButton.translatesAutoresizingMaskIntoConstraints = false
        
        favoritesButton.addTarget(self, action: #selector(favoritesButtonTapped(_:)), for: .touchUpInside)
        
        let barbutton = UIBarButtonItem(customView: favoritesButton)
        self.navigationItem.rightBarButtonItems?.append(barbutton)
        
        NSLayoutConstraint.activate([
            favoritesButton.widthAnchor.constraint(equalToConstant: 25.0),
            favoritesButton.heightAnchor.constraint(equalToConstant: 25.0)
        ])
        
        favoritesButton.clipsToBounds = true
    }
    
    func addCouponTypeButton() {
        couponTypeButton.setTitle("...", for: .normal)
        couponTypeButton.titleLabel?.tintColor = .black
        couponTypeButton.contentMode = .scaleAspectFit
        couponTypeButton.translatesAutoresizingMaskIntoConstraints = false
        
        let barbutton = UIBarButtonItem(title: "...", style: .plain, target: self, action: #selector(moreTapped))
        
        let attributes = [NSAttributedString.Key.font:UIFont(name: "Helvetica Neue", size: 24)!,
                          NSAttributedString.Key.foregroundColor: UIColor.white]
        barbutton.setTitleTextAttributes(attributes, for: .normal)
        
        self.navigationItem.rightBarButtonItems?.append(barbutton)
        
        NSLayoutConstraint.activate([
            searchButton.widthAnchor.constraint(equalToConstant: 25.0),
            searchButton.heightAnchor.constraint(equalToConstant: 25.0)
        ])
        
        searchButton.clipsToBounds = true
    }
    
    @objc func moreTapped() {
        let couponTypeAlert = UIAlertController(title: "Estado", message: "Seleccione el estado:", preferredStyle: .actionSheet)
        let activeAtction = UIAlertAction(title: "Activos", style: .default) { (_) in
            self.couponStatusSelected(.ACTIVE)
        }
        couponTypeAlert.addAction(activeAtction)
        let inactiveAtction = UIAlertAction(title: "Inactivos", style: .default) { (_) in
            self.couponStatusSelected(.INACTIVE)
        }
        couponTypeAlert.addAction(inactiveAtction)
        let cancelAtction = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        couponTypeAlert.addAction(cancelAtction)
        self.present(couponTypeAlert, animated: true, completion: nil)
    }
    
    func couponStatusSelected(_ status: CouponService.Status) {
        print(status)
        fatalError("override")
    }
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        guard let mainVC = self.navigationController?.tabBarController?.parent as? MainScreenViewController else {
          return
        }
        if(!mainVC.menuVisible){
            return
        }
        mainVC.toggleSideMenu(fromViewController: self)
    }
    
    @IBAction func infoButtonTapped(_ sender: Any){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let colorsDescVC = storyboard.instantiateViewController(withIdentifier: "ColorsDescVC") as? ColorsDefinitionViewController {
            colorsDescVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(colorsDescVC, animated: false, completion: nil)
        }
    }
    
    @IBAction func searchButtonTapped(_ sender: Any){
        print("search")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let searchVC = storyboard.instantiateViewController(withIdentifier: "SearchVC") as? SearchViewController {
            guard let mainVC = self.navigationController?.tabBarController?.parent as? MainScreenViewController else {
                return
            }
            mainVC.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
    
    @IBAction func favoritesButtonTapped(_ sender: Any){
        print("favorites")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let favoritesVC = storyboard.instantiateViewController(withIdentifier: "FavoritesVC") as? FavoritesViewController {
            guard let mainVC = self.navigationController?.tabBarController?.parent as? MainScreenViewController else {
                return
            }
            mainVC.navigationController?.pushViewController(favoritesVC, animated: true)
        }
    }
    
    @IBAction func sortAndFilterButtonTapped(_ sender: Any){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let sortAndFilterVC = storyboard.instantiateViewController(withIdentifier: "SortAndFilterVC") as? SortAndFilterViewController {
            sortAndFilterVC.delegate = self
            sortAndFilterVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(sortAndFilterVC, animated: false, completion: nil)
        }
    }
    
    @objc func handlePan(_ recognizer: UIPanGestureRecognizer){
        let translation = recognizer.translation(in: self.view)
        
        guard let mainVC = self.navigationController?.tabBarController?.parent as? MainScreenViewController else {
            return
        }
        
        if(recognizer.state == .ended || recognizer.state == .failed || recognizer.state == .cancelled){
            
            if(mainVC.menuVisible){
                // user finger moved to left before ending drag
                if(translation.x < 0){
                    // toggle side menu (to fully hide it)
                    mainVC.toggleSideMenu(fromViewController: self)
                }
            } else {
                // user finger moved to right and more than 100pt
                if(translation.x > 100.0){
                    // toggle side menu (to fully show it)
                    mainVC.toggleSideMenu(fromViewController: self)
                } else {
                    // user finger moved to right but too less
                    // hide back the side menu (with animation)
                    mainVC.view.layoutIfNeeded()
                    UIView.animate(withDuration: 0.5, animations: {
                        mainVC.sideMenuViewLeadingConstraint.constant = 0 - mainVC.sideMenuContainer.frame.size.width
                        mainVC.contentViewLeadingConstraint.constant = 0
                        mainVC.view.layoutIfNeeded()
                    })
                }
            }
            
            // early return so code below won't get executed
            return
        }
        
        
        if(!mainVC.menuVisible && translation.x > 0.0 && translation.x <= mainVC.sideMenuContainer.frame.size.width) {
            mainVC.sideMenuViewLeadingConstraint.constant = 0 - mainVC.sideMenuContainer.frame.size.width + translation.x
            
            mainVC.contentViewLeadingConstraint.constant = 0// + translation.x
        }
        
        if(mainVC.menuVisible && translation.x >= 0 - mainVC.sideMenuContainer.frame.size.width && translation.x < 0.0) {
            mainVC.sideMenuViewLeadingConstraint.constant = 0 + translation.x
            
            mainVC.contentViewLeadingConstraint.constant = 0//mainVC.sideMenuContainer.frame.size.width + translation.x
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBarAppearance()
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
  @IBAction func menuButtonTapped(_ sender: Any){
    if let mainVC = self.navigationController?.tabBarController?.parent as? MainScreenViewController {
        mainVC.toggleSideMenu(fromViewController: self)
    }
  }
    
    @objc func appMovedToForeground() {
        if (self.viewIfLoaded?.window != nil) {
            appMovedToForegroundChild()
        }
    }
    
    func appMovedToForegroundChild() {
        fatalError("redefine in child")
    }
    
    func sortAndItemChosenForBenefits(sortType: SortAndFilterViewController.SortType, category: Category) {
        fatalError("redefine in benefit child")
    }
    
    func resetSortAndFilterChosen() {
        fatalError("redefine in benefit child")
    }
    
}

extension ContentViewController: SortAndFilterProtocol {
    
    func resetSortAndFilter() {
        self.resetSortAndFilterChosen()
    }
    
    func sortAndItemChosen(_ sort: SortAndFilterViewController.SortType, _ cat: Category) {
        print("sortAndItemChosen")
        self.sortAndItemChosenForBenefits(sortType: sort, category: cat)
    }
}


extension ContentViewController: UIPopoverPresentationControllerDelegate {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCouponTypeSelector" {
            let popoverViewController = segue.destination
            popoverViewController.popoverPresentationController!.delegate = self
        }
    }

    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        setAlphaOfBackgroundViews(alpha: 1)
    }

    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        setAlphaOfBackgroundViews(alpha: 0.7)
    }

    func setAlphaOfBackgroundViews(alpha: CGFloat) {
        let statusBarWindow = NavigationController.shared.appDelegate?.window//UIApplication.shared.value(forKey: "statusBarWindow") as? UIWindow
        UIView.animate(withDuration: 0.2) {
            statusBarWindow?.alpha = alpha;
            self.view.alpha = alpha;
            self.navigationController?.navigationBar.alpha = alpha;
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        // Tells iOS that we do NOT want to adapt the presentation style for iPhone
        return .none
    }
}
