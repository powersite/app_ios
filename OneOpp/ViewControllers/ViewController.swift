//
//  ViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 18/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var versionLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("second launch screen")
        
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        self.versionLbl.text = appVersion
        
        Utils.startProgressAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Utils.stopProgressAnimation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: animated)
//    }


}

