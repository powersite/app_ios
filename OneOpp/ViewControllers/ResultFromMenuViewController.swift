//
//  ResultFromMenuViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 12/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class ResultFromMenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func acceptTouched(_ sender: Any) {
        guard let nav = self.navigationController else {
            return
        }
        NavigationController.returnToMain(nav: nav)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
