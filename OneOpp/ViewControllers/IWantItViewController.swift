//
//  IWantItViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 10/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

protocol IWantACouponProtocol: class {
    func couponAccepted()
}

class IWantItViewController: UIViewController {

    @IBOutlet var headerView: UIView!
    @IBOutlet var discount: UILabel!
    @IBOutlet var businessName: UILabel!
    
    @IBOutlet var centerYConstraint: NSLayoutConstraint!
    @IBOutlet var centerXConstraint: NSLayoutConstraint!
    weak var delegate: IWantACouponProtocol?
    
    public var benefit: Benefit?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerView.addGradientBackground("#F3C518", "#EF4814")
        
        guard let discount = benefit?.discount, let disc = discount.label, let name = benefit?.title, let businessName = benefit?.business?.name else {
            return
        }
        
        self.discount.text = "\(disc) en \(name)"
        self.businessName.text = businessName
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    @IBAction func accept(_ sender: Any) {
        dismiss(animated: false, completion: nil)
        delegate?.couponAccepted()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
