//
//  ColorsDefinitionViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 06/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class ColorsDefinitionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        headerView.addGradientBackground("#F3C518", "#EF4814")
    }
    
    @IBOutlet var headerView: UIView!
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
