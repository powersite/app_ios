//
//  RegisterViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 23/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class RegisterViewController: UIViewController {

    @IBOutlet var borderedViews: [UIView]!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var toggleButton: UIButton!
    @IBOutlet var nameField: UITextField!
    @IBOutlet var emailField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toggleButton.isEnabled = false
        configBorderedViews()
        passwordField.setPasswordEye()
    }
    
    func configBorderedViews() {
        for view in self.borderedViews {
            view.borderColor = .black
            view.borderWidth = 1.0
            view.cornerRadius = 20
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        nameField.setError("", show: false)
        nameField.setError("", show: false)
        passwordField.setError("", show: false)
    }

    @IBAction func togglePasswordField(_ sender: Any) {
        self.passwordField.isSecureTextEntry.toggle()
        displayToggleButtonProperly()
    }
    
    func displayToggleButtonProperly() {
        if self.passwordField.isSecureTextEntry {
            self.toggleButton.setImage(UIImage(named: "show-pass"), for: .normal)
        } else {
            self.toggleButton.setImage(UIImage(named: "hide-pass"), for: .normal)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func doRegistration(_ sender: Any) {
//        self.performSegue(withIdentifier: "showSuccessRegistrationScreen", sender: nil)
//        if !Validator.checkEmptyFields([nameField, passwordField, emailField]) { return }
        var validName = true
        if !TextfieldValidator.checkEmpty(nameField) || !TextfieldValidator.checkMinLength(nameField, 3, errorMsg: "El nombre debe contener al menos 3 caracteres") {
            validName = false
        }
        
        var validEmail = true
        if !TextfieldValidator.checkEmpty(emailField) || !TextfieldValidator.isValidEmail(field: emailField) {
            validEmail = false
        }
        
        var validPass = true
        if !TextfieldValidator.checkEmpty(passwordField) || !TextfieldValidator.checkMinLength(passwordField, 4, errorMsg: "El password es muy corto") {
            validPass = false
        }
        
        if !(validName && validEmail && validPass) { return }
        
        let name = nameField.text!
        let email = emailField.text!
        let pass = passwordField.text!
        
        Utils.startProgressAnimation()
        UserController.shared.register(email: email, name: name, password: pass, completionSuccess: {
            print("success")
            Utils.stopProgressAnimation()
            self.performSegue(withIdentifier: "showSuccessRegistrationScreen", sender: nil)
        }) { (error) in
            print(error?.message)
            Utils.stopProgressAnimation()
            self.showAlert(title: error?.error, msg: error?.message)
        }
    }
    
}

extension RegisterViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          if(textField == passwordField){
              if let text = textField.text as NSString? {
//                  if textField.rightView?.tag == 998 {
                      let updatedText = text.replacingCharacters(in: range, with: string)
                      (textField.rightView as? UIButton)?.isEnabled = updatedText.count > 0
//                  }
                if textField.rightView?.tag == 999 {
                if !TextfieldValidator.checkEmpty(passwordField, futureText: updatedText) || !TextfieldValidator.checkMinLength(passwordField, 4, errorMsg: "El password es muy corto", futureText: updatedText) {
//                    passwordField.setError()
                    }
                    
                }
              }
          }
          return true
      }
}
