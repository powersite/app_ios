//
//  NotificationsByBusinessViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 04/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationsByBusinessViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    var businesses: [Business]?
    var bannedBusinesses = [Business]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        tableView.register(UINib(nibName: "NotificationByBusinessTableViewCell", bundle: nil), forCellReuseIdentifier: NotificationByBusinessTableViewCell.NotificationCellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadBusinesses()
    }
    
    func loadBusinesses() {
        Utils.startProgressAnimation()
        BusinessesController.getBlackListedBusinesses { (businesses) in
            self.businesses = businesses
            self.businesses?.forEach({ (business) in
                if let _ = business.blackListed {
                    self.bannedBusinesses.append(business)
                }
            })
            self.tableView.reloadData()
            Utils.stopProgressAnimation()
        } completionError: {
            print("error")
            Utils.stopProgressAnimation()
        }
    }
    
    @IBAction func acceptAction(_ sender: Any) {
        Utils.startProgressAnimation()
        BusinessService.addToBlackList(businesses: self.bannedBusinesses) {
            print("banned")
            Utils.stopProgressAnimation()
        } completionError: { (_) in
            print("error banning")
            Utils.stopProgressAnimation()
        }

    }

}

extension NotificationsByBusinessViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return businesses?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotificationByBusinessTableViewCell.NotificationCellId) as? NotificationByBusinessTableViewCell else {
            fatalError()
        }
        if let business = self.businesses?[indexPath.row] {
            var newBusiness = business
            newBusiness.blackListed = bannedBusinesses.contains(where: { (bannedBusiness) -> Bool in
                return bannedBusiness.id == business.id
            })
            cell.configureBusiness(newBusiness)
            cell.notificationByBusinessDelegate = self
        }
        return cell
    }
}

extension NotificationsByBusinessViewController: NotificationByBusinessCellDelegate {
    func selectionChanged(business: Business, selection: Bool) {
        
        if selection {
            // append
            self.bannedBusinesses.append(business)
        } else {
            if let businessIx = self.bannedBusinesses.firstIndex(where: { (busi) -> Bool in
                return busi.id == business.id
            }) {
                self.bannedBusinesses.remove(at: businessIx)
            }
        }
    }
}
