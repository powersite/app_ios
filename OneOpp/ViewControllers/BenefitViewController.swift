//
//  BenefitViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import CoreLocation

class BenefitViewController: ContentViewController {

    @IBOutlet var mainStack: UIStackView!
    @IBOutlet var firstView: UIView!
    @IBOutlet var secondView: UIView!
    private enum CellIdentifiers {
      static let list = "BenefitCell"
    }
    @IBOutlet var categFilterLbl: UILabel!
    @IBOutlet var sortAndFilterViewHeight: NSLayoutConstraint!
    
    @IBOutlet var indicatorView: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    
    var site: String!
    var categoryId: Int?
    var coreLocationService: CLLocationManager?
    var location: CLLocation?
    
    private var benefitsController: BenefitsController!
    
    private var shouldShowLoadingCell = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let notificationCenter = NotificationCenter.default
//        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)

        tableView.register(UINib(nibName: "BenefitTableViewCell", bundle: nil), forCellReuseIdentifier: BenefitTableViewCell.benefitId)
        tableView.prefetchDataSource = self
        
//        loadBenefits()
        
        self.tableView.allowsSelection = true
        self.sortAndFilterViewHeight.constant = 0
        
        configureCoreLocation()
    }
    
    func configureCoreLocation() {
        coreLocationService = CLLocationManager()
        coreLocationService!.delegate = self
        coreLocationService!.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        coreLocationService!.requestWhenInUseAuthorization()
    }
    
    override func appMovedToForegroundChild() {
//        loadBenefits()
        requestUpdateLocationIfLocationEnabled()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        tapGestureRecognizer?.cancelsTouchesInView = false
        tapGestureRecognizer?.isEnabled = false
        print("didAppear")
//        loadBenefits()
        requestUpdateLocationIfLocationEnabled()
        
        addSearchButton()
        addSortAndFilterButton()
        addFavoritesButton()
//        addInfoButton()
    }
    
    func loadBenefits() {
        guard let location = self.location else {
            return
        }
        
        Utils.startProgressAnimation()

        tableView.isHidden = true
      
        let getBenefitServiceRequest = GetBenefitServiceRequest()
        getBenefitServiceRequest.page = 0
        getBenefitServiceRequest.size = 10
        getBenefitServiceRequest.orderBy = .DATE
        getBenefitServiceRequest.category_id = self.categoryId
        getBenefitServiceRequest.latitud = "\(location.coordinate.latitude)"
        getBenefitServiceRequest.longitud = "\(location.coordinate.longitude)"
        benefitsController = BenefitsController(request: getBenefitServiceRequest, delegate: self)

        benefitsController.fetchBenefits()
    }
    
    override func sortAndItemChosenForBenefits(sortType: SortAndFilterViewController.SortType, category: Category) {
        print("sortAndItemChosen in child")
        if category.id > -1 {
            self.categoryId = category.id
            self.setCategoryLabel(sort: sortType, category: category.name)
        } else {
            self.categoryId = nil
            self.setCategoryLabel(sort: sortType, category: nil)

        }
        
        self.sortAndFilterViewHeight.constant = 45
        
        loadBenefits()
    }
    
    override func resetSortAndFilterChosen() {
        self.categFilterLbl.text = ""
        self.sortAndFilterViewHeight.constant = 0
        
        self.categoryId = nil
        loadBenefits()
    }
    
    func setCategoryLabel(sort: SortAndFilterViewController.SortType, category: String?) {
        var ssort: String = ""
        switch sort {
        case .best:
            ssort = "Mejor descuento"
        case .latest:
            ssort = "Recientes"
        case .nearest:
            ssort = "Más cercanos"
        }
        if let c = category {
            self.categFilterLbl.text = "\(ssort) en \(c)"
        } else {
            self.categFilterLbl.text = "\(ssort)"
        }
    }
    
    @IBAction func resetSortAndFilter(_ sender: Any) {
        resetSortAndFilterChosen()
    }
    
}

extension BenefitViewController:UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: isLoadingCell) {
            benefitsController.fetchBenefits()
        }
    }
}

extension BenefitViewController:BenefitsControllerDelegate {
    func onFetchCompleted(with newIndexPathsToReload: [IndexPath]?) {
        
        if benefitsController.totalCount > 0 {
            secondView.isHidden = true
            firstView.isHidden = false
        } else {
            secondView.isHidden = false
            firstView.isHidden = true
        }
      // 1
      guard let newIndexPathsToReload = newIndexPathsToReload else {
        Utils.stopProgressAnimation()
        tableView.isHidden = false
        tableView.reloadData()
        return
      }
      // 2
      let indexPathsToReload = visibleIndexPathsToReload(intersecting: newIndexPathsToReload)
      tableView.reloadRows(at: indexPathsToReload, with: .automatic)
    }
    
    func onFetchFailed(with reason: String) {
        Utils.stopProgressAnimation()
        print("Warning. Reason: \(reason)")
    }
}

extension BenefitViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return benefitsController?.totalCount ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.list, for: indexPath) as! BenefitTableViewCell
        // 2
        if isLoadingCell(for: indexPath) {
          cell.configure(with: .none)
        } else {
            cell.configure(with: benefitsController.benefit(at: indexPath.row))
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let benefit = benefitsController.benefit(at: indexPath.row)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let benefitDetailVC = storyboard.instantiateViewController(withIdentifier: "BenefitDetail") as? BenefitDetailViewController {
            benefitDetailVC.benefit = benefit
            NavigationController.navigateFromTabBarVC(self, vc: benefitDetailVC)
        }
    }
    
    func askUserToAllowLocation() {
        // Hide table
        secondView.isHidden = false
        firstView.isHidden = true
        
        self.showAlert(title: "Se necesita autorización de ubicación", msg: "Por favor, autorice la ubicación desde las opciones de configuración del dispositivo.")
    }
}

private extension BenefitViewController {
  func isLoadingCell(for indexPath: IndexPath) -> Bool {
    return indexPath.row >= benefitsController.currentCount
  }
  
  func visibleIndexPathsToReload(intersecting indexPaths: [IndexPath]) -> [IndexPath] {
    let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows ?? []
    let indexPathsIntersection = Set(indexPathsForVisibleRows).intersection(indexPaths)
    return Array(indexPathsIntersection)
  }
}

extension BenefitViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
       
       requestUpdateLocationIfLocationEnabled()
    }
    
    func requestUpdateLocationIfLocationEnabled() {
        guard CLLocationManager.locationServicesEnabled() else {
            print("CL disabled")
            self.askUserToAllowLocation()
            return
        }

        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse, .authorizedAlways:
            Utils.startProgressAnimation()
            self.coreLocationService!.requestLocation()
//            self.coreLocationService?.startUpdatingLocation()
//            self.location = self.coreLocationService?.location
            self.loadBenefits()
        case .denied, .restricted, .notDetermined:
            print("CL NOT authorized")
            self.askUserToAllowLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.location = location
            self.loadBenefits()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("CL location manager didFailWithError")
        Utils.stopProgressAnimation()
    }
}
