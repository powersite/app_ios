//
//  MainScreenViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 30/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import Foundation

class MainScreenViewController: UIViewController {
    @IBOutlet var contentViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var sideMenuContainer: UIView!
    @IBOutlet var sideMenuViewLeadingConstraint: NSLayoutConstraint!
    
    var menuVisible = false
    
    var sideMenuViewController : MenuViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
//        print("User name: \(UserController.shared.user?.name ?? "")")
//        print("User email: \(UserController.shared.user?.email ?? "")")
        for childViewController in self.children {
          if let sideMenuVC = childViewController as? MenuViewController {
            sideMenuViewController = sideMenuVC
            break
          }
        }
        
        sideMenuViewLeadingConstraint.constant = 0 - self.sideMenuContainer.frame.size.width
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func toggleSideMenu(fromViewController: UIViewController) {
        if(menuVisible){
            UIView.animate(withDuration: 0.5, animations: {
                // hide the side menu to the left
                self.sideMenuViewLeadingConstraint.constant = 0 - self.sideMenuContainer.frame.size.width
                // move the content view (tab bar controller) to original position
                self.contentViewLeadingConstraint.constant = 0
                self.view.layoutIfNeeded()
            })
        } else {
            self.sideMenuViewController?.currentActiveNav = self.navigationController//fromViewController.navigationController
            
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.5, animations: {
                // move the side menu to the right to show it
                self.sideMenuViewLeadingConstraint.constant = 0
                // move the content view (tab bar controller) to the right
                self.contentViewLeadingConstraint.constant = 0//self.sideMenuContainer.frame.size.width
                self.view.layoutIfNeeded()
            })
        }
        
        menuVisible = !menuVisible
        self.children.forEach { (vc) in
            if vc.isKind(of: TabBarViewController.self) {
                if let tbvc = (vc as? TabBarViewController) {
                    if let selectedNVC = tbvc.selectedViewController {
                        if let currentNavVC = selectedNVC as? UINavigationController {
                            let selectedVC = currentNavVC.viewControllers.first
                           
                            if let cuponVC = selectedVC! as? CouponsViewController {
                                cuponVC.panGestureRecognizer?.isEnabled = menuVisible
                            }
                            
//                            if let currentTabVC = selectedVC! as? ContentViewController {
//                                currentTabVC.tapGestureRecognizer?.cancelsTouchesInView = menuVisible
//                                currentTabVC.tapGestureRecognizer?.isEnabled = menuVisible
//                                 modify tapgesture recognizer not only for selected vc but for all
                                if let tbvc = (vc as? TabBarViewController) {
                                    tbvc.children.forEach { (ch) in
                                        if let navC = ch as? UINavigationController {
                                            if let cVc = navC.viewControllers.first as? ContentViewController {
//                                                    cVc.tapGestureRecognizer?.cancelsTouchesInView = menuVisible
                                                cVc.tapGestureRecognizer?.isEnabled = menuVisible
                                            }
                                        }
                                    }
                                }
//                            }
                        }
                    }
                }
            }
        }
    }
    
    func hideSideMenu() {
      if(menuVisible){
        UIView.animate(withDuration: 0.5, animations: {
            self.sideMenuViewLeadingConstraint.constant = 0 - self.sideMenuContainer.frame.size.width
            self.contentViewLeadingConstraint.constant = 0
            self.view.layoutIfNeeded()
        })
      
        menuVisible = !menuVisible
      }
    }
    
    func shareApp() {
        print("share")
        
        let activityViewController = UIActivityViewController(activityItems: [Utils.Constants.DOWNLOAD_URL], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

        self.present(activityViewController, animated: true, completion: nil)
    }

}
