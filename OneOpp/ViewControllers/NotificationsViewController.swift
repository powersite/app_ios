//
//  NotificationsViewController.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 07/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {
    
    @IBOutlet var switchControl: UISwitch!
    @IBOutlet var tableView: UITableView!
    private var userCategories: [Category]?
    var currentAllowedCategories = [Category]()
    var areCategoriesExpanded: Bool = true
    var bannedCount: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        self.switchControl.isOn = UserController.shared.getNotificationPermission()
        
        self.tableView.register(UINib(nibName: "NotificationByCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: NotificationByCategoryTableViewCell.cellId)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadData()
        

    }
    
    func loadData() {
        let group = DispatchGroup()
        Utils.startProgressAnimation()
        group.enter()
        CategoriesController.getAuthorizedCategories { (categories) in
            self.userCategories = categories
            self.userCategories?.forEach({ (category) in
                if let _ = category.allowed {
                    self.currentAllowedCategories.append(category)
                }
            })
            group.leave()
        } completionError: {
            print("something went wrong")
            group.leave()
        }
        
        group.enter()
        BusinessService.getBlackListBusinesses { (businesses) in
            if let bannedBussinesses = businesses {
                self.bannedCount = bannedBussinesses.count
            } else {
                self.bannedCount = 0
            }
            group.leave()
        } completionError: {_ in
            print("error")
            group.leave()
        }
        
        group.notify(queue: .main){
            self.tableView.reloadData()
            Utils.stopProgressAnimation()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    private var showLoadingIndicator = true
    @IBAction func acceptAction(_ sender: Any) {
        let allowNotif = switchControl.isOn
        Utils.startProgressAnimation()
        let group = DispatchGroup()
        
        group.enter()
        UserController.shared.sendConfigurationForUser(allow: allowNotif, completionSuccess: {
            group.leave()
        }) { (error) in
            self.showAlert(title: error?.error, msg: error?.message)
        }
        
        group.enter()
        NotificationCategoriesService.addCategoriesToNotification(categories: self.currentAllowedCategories) {
            group.leave()
        } completionError: { (error) in
            self.showAlert(title: error.error, msg: error.message)
        }
        
        group.notify(queue: .main) {
            Utils.stopProgressAnimation()
            self.performSegue(withIdentifier: "ShowNotificationsSuccess", sender: nil)
        }
        
    }
    
    @objc
    func handleNotificationsByBusiness() {
        print("Comercios tapped")
        self.performSegue(withIdentifier: "ShowNotificationByBusiness", sender: nil)
    }
    
    @objc
    func handleNotificationsByCategory() {
        guard let categories = self.userCategories else {
            return
        }
        
        var indexPaths = [IndexPath]()
        for index in categories.indices {
            let indexPath = IndexPath(row: index, section: 1)
            indexPaths.append(indexPath)
        }
        
        self.areCategoriesExpanded.toggle()
        
        if areCategoriesExpanded {
            self.tableView.insertRows(at: indexPaths, with: .fade)
        } else {
            self.tableView.deleteRows(at: indexPaths, with: .fade)
        }
    }
}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotificationByCategoryTableViewCell.cellId) as? NotificationByCategoryTableViewCell else {
            fatalError()
        }
        if let cat = self.userCategories?[indexPath.row] {
            var newCateg = cat
            newCateg.allowed = currentAllowedCategories.contains(where: { (allowedCateg) -> Bool in
                return allowedCateg.id == cat.id
            })
            cell.configureCategory(newCateg)
            cell.notificationByCategoryDelegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let button = UIButton()
        let title: String
        if section == 0 {
            title = "Comercios (\(self.bannedCount))"
            button.addTarget(self, action: #selector(handleNotificationsByBusiness), for: .touchDown)
        } else {
            title = "Categorías"
            button.addTarget(self, action: #selector(handleNotificationsByCategory), for: .touchDown)
        }
        
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: self.tableView.frame.size.width - 25, bottom: 0, right: 0)
        
        button.contentHorizontalAlignment = .leading
//        if section == 0 {
            button.setImage(UIImage(named: "right-arrow-icon"), for: .normal)
        /*} else {
            if areCategoriesExpanded {
                button.setImage(UIImage(named: "right-arrow-icon"), for: .normal)
            } else {
                button.setImage(UIImage(named: "down-arrow-icon"), for: .normal)
            }
        }*/
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .lightGray
        button.setTitle(title, for: .normal)
        button.titleLabel?.text = title
        return button
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
        case 1:
            if self.areCategoriesExpanded {
                return self.userCategories?.count ?? 0
            } else {
                return 0
            }
        default:
            return 0
        }
    }
}
extension NotificationsViewController: NotificationByCategoryCellDelegate {
    func selectionChanged(category: Category, selection: Bool) {
        
        if selection {
            // append
            self.currentAllowedCategories.append(category)
        } else {
            if let categoryIx = self.currentAllowedCategories.firstIndex(where: { (categ) -> Bool in
                return categ.id == category.id
            }) {
                self.currentAllowedCategories.remove(at: categoryIx)
            }
        }
    }
}
