//
//  StepCollectionViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 07/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class StepCollectionViewCell: UICollectionViewCell {
    @IBOutlet var stepImage: UIImageView!
    @IBOutlet var stepDescription: UILabel!
    
}
