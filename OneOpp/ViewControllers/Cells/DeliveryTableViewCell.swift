//
//  DeliveryTableViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 10/07/2021.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import SDWebImage

class DeliveryTableViewCell: UITableViewCell {
    @IBOutlet var benefitImage: UIImageView!
    @IBOutlet var benefitTitle: UILabel!
    @IBOutlet var category: UILabel!
    @IBOutlet var benefitDesc: UILabel!
    @IBOutlet var payMethod: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var benefitDelivery: UILabel!
    static let deliveryCellId = "DeliveryCell"
    
    override func prepareForReuse() {
      super.prepareForReuse()
      
      configure(with: .none)
    }
    
    override func awakeFromNib() {
      super.awakeFromNib()
      
      activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .green
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with delivery: Delivery?) {
        if let benefit = delivery?.benefitDTO {
            benefitTitle.text = benefit.business?.name ?? ""
            if let categ = benefit.category, let categName = categ.name {
                category?.text = categName
            } else {
                category?.text = ""
            }
            benefitDesc.text = benefit.title ?? ""
//            if let delivery = benefit.deliveryByCoupon {
            self.benefitDelivery.text = delivery?.deliveryStatusName
            self.benefitDelivery.superview?.isHidden = false
            if let price = benefit.price {
                self.price.text = "$\(String(describing: Int(price)))"
            } else {
                self.price.text = ""
            }
            switch delivery?.deliveryStatusId {
            case 1:
                self.benefitDelivery.superview?.backgroundColor = UIColor(red: 31/255, green: 201/255, blue: 91/255, alpha: 1)
            case 2:
                self.benefitDelivery.superview?.backgroundColor = UIColor(red: 242/255, green: 25/255, blue: 41/255, alpha: 1)
            case 3:
                self.benefitDelivery.superview?.backgroundColor = UIColor(red: 215/255, green: 90/255, blue: 30/255, alpha: 1)
            default:
                self.benefitDelivery.superview?.isHidden = true
            }
//            } else {
//                self.benefitDelivery.superview?.isHidden = true
//            }
            if let imgName = benefit.business?.thumbnail {
                let imgUrl = NetworkingController.Constants.IMAGES_PATH + imgName
                self.benefitImage.sd_setImage(with: URL(string: imgUrl)) { (image, _, _, _) in
                }
            }
            activityIndicator.stopAnimating()
        } else {
            activityIndicator.startAnimating()
        }
    }

}
