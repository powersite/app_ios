//
//  BenefitTableViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 24/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import SDWebImage

class BenefitTableViewCell: UITableViewCell {
    @IBOutlet var benefitImage: UIImageView!
    @IBOutlet var benefitTitle: UILabel!
    @IBOutlet var category: UILabel!
    @IBOutlet var benefitDesc: UILabel!
    @IBOutlet var oneHand: UIImageView!
    @IBOutlet var benefitDiscountLbl: UILabel!
    @IBOutlet var benefitDiscountView: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var qTotal: UILabel!
    @IBOutlet var qTaken: UILabel!
    @IBOutlet var qExchanged: UILabel!
    @IBOutlet var deliveryIcon: UIImageView!
    @IBOutlet var benefitDelivery: UILabel!
    @IBOutlet var favoriteButton: UIButton!
    static let benefitId = "BenefitCell"
    
    weak var favDelegate: FavoritesDelegate?
    
    var benefit: Benefit?
    
    override func prepareForReuse() {
      super.prepareForReuse()
      
      configure(with: .none)
    }
    
    override func awakeFromNib() {
      super.awakeFromNib()
      
      activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .green
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(with benefit: Benefit?) {
        if let benefit = benefit {
            self.benefit = benefit
            benefitTitle.text = benefit.business?.name ?? ""
            category?.text = benefit.category?.name ?? ""
            benefitDesc.text = benefit.title ?? ""
            benefitDiscountLbl.text = benefit.discount?.label
            deliveryIcon.isHidden = !(benefit.delivery ?? true)
            favoriteButton.isSelected = benefit.favorite ?? false
            if let delivery = benefit.deliveryByCoupon {
                self.benefitDelivery.text = delivery.deliveryStatusName
                self.benefitDelivery.superview?.isHidden = false
                switch delivery.deliveryStatusId {
                case 1:
                    self.benefitDelivery.superview?.backgroundColor = UIColor(red: 31/255, green: 201/255, blue: 91/255, alpha: 1)
                case 2:
                    self.benefitDelivery.superview?.backgroundColor = UIColor(red: 242/255, green: 25/255, blue: 41/255, alpha: 1)
                case 3:
                    self.benefitDelivery.superview?.backgroundColor = UIColor(red: 215/255, green: 90/255, blue: 30/255, alpha: 1)
                default:
                    self.benefitDelivery.superview?.isHidden = true
                }
            } else {
                self.benefitDelivery.superview?.isHidden = true
            }
            if let colorHexString = benefit.discount?.color, let colorDisc = UIColor(hexString: "#\(colorHexString)") {
                benefitDiscountView.backgroundColor = colorDisc
            }
            if let imgName = benefit.business?.thumbnail {
                let imgUrl = NetworkingController.Constants.IMAGES_PATH + imgName
                self.benefitImage.sd_setImage(with: URL(string: imgUrl)) { (image, _, _, _) in
                }
            }
            qTotal.text = "\(benefit.quantity ?? 0)"
            qTaken.text = "\(benefit.quantityTaken ?? 0)"
            qExchanged.text = "\(benefit.quantityExchanged ?? 0)"
            
            oneHand.isHidden = (benefit.quantityTaken ?? 0) <= 0 
            activityIndicator.stopAnimating()
        } else {
            activityIndicator.startAnimating()
        }
    }
    
    @IBAction func toggleFavorite(_ sender: Any) {
        guard let benefit = self.benefit else {
            return
        }
        self.favoriteButton.isSelected.toggle()
        self.benefit?.favorite = self.favoriteButton.isSelected

        if favoriteButton.isSelected {
            FavoritesService.setFavorite(benefitId: benefit.idBeneficio) {
                print("favorite set")
                self.favDelegate?.favoritesChanged()
            } completionError: { (_) in
                print("favorite set - error")
            }
        } else {
            FavoritesService.deleteFavorite(benefitId: benefit.idBeneficio) {
                print("favorite deleted")
                self.favDelegate?.favoritesChanged()
            } completionError: { (_) in
                print("favorite delete - error")
            }

        }

    }
    
}
@objc protocol FavoritesDelegate {
    @objc func favoritesChanged()
}
