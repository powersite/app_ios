//
//  NotificationByBusinessTableViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 04/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import UIKit

protocol NotificationByBusinessCellDelegate: NSObject {
    func selectionChanged(business: Business, selection: Bool)
}
class NotificationByBusinessTableViewCell: UITableViewCell {
    static let NotificationCellId = "NotificationCellId"
    @IBOutlet var businessImage: UIImageView!
    @IBOutlet var businessName: UILabel!
    @IBOutlet var switchSelection: UISwitch!
    var business: Business?
    
    weak var notificationByBusinessDelegate: NotificationByBusinessCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureBusiness(_ business: Business) {
        self.business = business
        
        if let imgName = business.thumbnail {
            let imgUrl = imgName
            self.businessImage.sd_setImage(with: URL(string: imgUrl)) { (image, _, _, _) in
            }
        }
        if let blackListed = business.blackListed {
            self.switchSelection.isOn = blackListed
        } else {
            self.switchSelection.isOn = false
        }
        self.businessName.text = business.name
    }
    
    @IBAction func switchValueChanged(_ sender: Any) {
        guard let business = self.business else {
            return
        }
        self.business?.blackListed = self.switchSelection.isOn
        
        self.notificationByBusinessDelegate?.selectionChanged(business: business, selection: switchSelection.isOn)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        if let business = self.business {
            configureBusiness(business)
        }
    }
    
}
