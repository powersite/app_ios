//
//  BenefitDetailTableViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 28/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class BenefitDetailTableViewCell: UITableViewCell {

    @IBOutlet var benefitImage: UIImageView!
    @IBOutlet var benefitDesc: UILabel?
    @IBOutlet var discountView: UIView?
    @IBOutlet var cardsStack: UIStackView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var endDate: Date?    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    var timer: Timer?
    
    func startClicking(endDate: Date) {
        self.endDate = endDate
//        self.benefitDesc?.text = endDate.offsetFrom(date: Date())
            tick()
//            startRotatingImage()
            
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
    }
    
    @objc func tick(){
        let now = Date()
        if self.endDate! < now {
            timer?.invalidate()
            startRotatingImage(stop: true)
            self.benefitDesc?.text = "Tiempo terminado"
        } else {
            self.benefitDesc?.text = self.endDate!.offsetFrom(date: now)
        }
    }
    
    var r = true
    func startRotatingImage(stop: Bool = false) {
        if stop {
            return
        }
        UIView.animate(withDuration: 2.0, animations: {
            if self.r {
                self.benefitImage.transform = CGAffineTransform(rotationAngle: (180.0 * .pi) / 180.0)
            } else {
                self.benefitImage.transform = CGAffineTransform(rotationAngle: 0)
            }
            self.r.toggle()
        }, completion: { (_) in
            self.startRotatingImage()
        })
    }
    
    override func prepareForReuse() {
        self.timer = nil
    }

}
