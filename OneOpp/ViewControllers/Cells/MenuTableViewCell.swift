//
//  MenuTableViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 07/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var menuTitle: UILabel!
    
}
