//
//  FAQTableViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 10/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {

    @IBOutlet var question: UILabel!
    @IBOutlet var answer: UILabel!
    @IBOutlet var expandImg: UIImageView!
    @IBOutlet var detailHeight: NSLayoutConstraint!
    
//    var expanded: Bool = false {
//        didSet {
//            if self.expanded {
//                expandImg.image = UIImage(named: "")
//            } else {
//                expandImg.image = UIImage(named: "")
//            }
//        }
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        answer.isHidden = true
//        self.detailHeight.isActive = !isSelected
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        answer.isHidden = !selected
        
        self.detailHeight.isActive = !selected
        if selected {
            expandImg.image = UIImage(named: "collapse-icon")
        } else {
            expandImg.image = UIImage(named: "expand-icon")
        }
    }

}
