//
//  OnboardingCollectionViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 19/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class OnboardingCollectionViewCell: UICollectionViewCell {
    @IBOutlet var cellImage: UIImageView!
    @IBOutlet var cellTitle: UILabel!
    @IBOutlet var backColorsView: UIView!
    
    var firstGradientColor: UIColor?
    var secondGradientColor: UIColor?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.backColorsView?.layer.sublayers = []
    }
}
