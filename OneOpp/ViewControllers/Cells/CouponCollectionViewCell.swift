//
//  CouponCollectionViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 15/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import MapKit

class CouponCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: CouponCellProtocol?
    var coupon: Coupon?
    
    @IBOutlet var couponImage: UIImageView!
    @IBOutlet var couponName: UILabel!
    @IBOutlet var businessName: UILabel!
    @IBOutlet var category: UILabel!
    @IBOutlet var exchangeCoupon: UIButton!
    @IBOutlet var exchangeCouponTopShapeImg: UIImageView!
    @IBOutlet var exchangeCouponBottomShapeImg: UIImageView!
    
    func configureCoupon(_ coupon: Coupon, status: CouponService.Status) {
        self.coupon = coupon
        if let benefit = coupon.benefit {
            businessName.text = benefit.business?.name
            couponName.text = benefit.title ?? ""
            category?.text = benefit.category?.name ?? ""
            
            if let imgName = benefit.business?.thumbnail {
                let imgUrl = NetworkingController.Constants.IMAGES_PATH + imgName
                self.couponImage.sd_setImage(with: URL(string: imgUrl)) { (image, _, _, _) in
                }
            }
        }
        if status == .ACTIVE {
            self.exchangeCoupon.setTitle("CANJEAR", for: .normal)
            self.exchangeCoupon.addGradientBackground("#F8A64E", "#EA4E59")
            self.exchangeCouponTopShapeImg.image = UIImage(named: "coupon-button-separator")
            self.exchangeCouponBottomShapeImg.image = UIImage(named: "coupon-bottom")
        } else {
            if (self.coupon?.exchanged ?? false) {
                self.exchangeCoupon.setTitle("CANJEADO", for: .normal)
            } else {
                self.exchangeCoupon.setTitle("INACTIVO", for: .normal)
            }
            self.exchangeCoupon.addGradientBackground("#A9A9A9", "#7c7c7c")
            self.exchangeCouponTopShapeImg.image = UIImage(named: "coupon-button-sep-disabled")
            self.exchangeCouponBottomShapeImg.image = UIImage(named: "coupon-button-bottom-disabled")
        }
    }
    
    override func prepareForReuse() {
        self.coupon = nil
        businessName.text = ""
        couponName.text = ""
        category?.text = ""
        self.couponImage.image = nil
        exchangeCouponTopShapeImg.image = nil
        exchangeCouponBottomShapeImg.image = nil
    }
    
    @IBAction func exchangedTapped(_ sender: Any) {
        if let idC = self.coupon?.idCoupon {
            self.delegate?.exchangeCoupon(id: idC)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func carTapped(_ sender: Any) {
        guard let benefit = self.coupon?.benefit, let lat = benefit.business?.latitud?.toDouble(), let long = benefit.business?.longitud?.toDouble() else {
            return
        }
        let place = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long))
        let destination = MKMapItem(placemark: place)
        destination.name = benefit.business?.name
        let items = [destination]
        let options = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        MKMapItem.openMaps(with: items, launchOptions: options)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        guard let benefit = self.coupon?.benefit, let business = benefit.business, let busName = business.name, let disc = benefit.discount, let bTitle = benefit.title, let p = disc.label else {
            return
        }
        var share = Utils.Constants.BENEFIT_SHARE
        share = share.replacingOccurrences(of: "@business", with: busName)
        share = share.replacingOccurrences(of: "@discount", with: p)
        share = share.replacingOccurrences(of: "@description", with: bTitle)
        self.delegate?.shareCoupon(textToShare: share)
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        if let idC = self.coupon?.idCoupon {
            self.delegate?.deleteCoupon(id: idC)            
        }
    }
}

protocol CouponCellProtocol: NSObject {
//    func goTo(_ direction: String)
    func shareCoupon(textToShare: String)
    func deleteCoupon(id: Int)
    func exchangeCoupon(id: Int)
}
