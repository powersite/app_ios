//
//  NotificationTableViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 09/08/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet var discountBox: UIView!
    @IBOutlet var discountLbl: UILabel!
    @IBOutlet var notifTitle: UILabel!
    @IBOutlet var notifSubtitle: UILabel!
    @IBOutlet var notifDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureWithNotification(_ notification: NotificationModel) {
        
        /*
        if let colors = screen["colors"] as? [String], colors.count == 2 {
            
            cell.backColorsView.addGradientBackground(colors.first!, colors.last!)
        }
        if let text = screen["text"] as? String {
            cell.cellTitle.text = text
        }
         
         {
            "title": "Aprovecha este descuento",
            "description": "Hamburguesa Vegetal",
            "label": "50%",
            "color": "EE6323",
            "startDate": 1599099840000,
            "endDate": 1600914240000
        }
         
         */
        
        
        if let colorHexString = notification.color, let colorDisc = UIColor(hexString: colorHexString) {
            discountBox.backgroundColor = colorDisc
        }
        discountLbl.text = notification.discount
        notifTitle.text = notification.business
        notifSubtitle.text = notification.title
        if let enddate = notification.enddate, let dendDate = enddate.getDate() {
            notifDescription.text = "\(dendDate.toStringWithTime())"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
