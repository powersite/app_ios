//
//  NotificationByCategoryTableViewCell.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 05/03/2021.
//  Copyright © 2021 Vanina Geneiro. All rights reserved.
//

import UIKit

protocol NotificationByCategoryCellDelegate: NSObject {
    func selectionChanged(category: Category, selection: Bool)
}
class NotificationByCategoryTableViewCell: UITableViewCell {
    static let cellId = "NotificationByCategoryCell"
    @IBOutlet var categoryTitle: UILabel!
    @IBOutlet var switchSelection: UISwitch!
    var category: Category?
    
    weak var notificationByCategoryDelegate: NotificationByCategoryCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCategory(_ category: Category) {
        self.category = category
        
        if let allowed = category.allowed {
            self.switchSelection.isOn = allowed
        } else {
            self.switchSelection.isOn = false
        }
        self.categoryTitle.text = category.name
    }

    @IBAction func switchValueChanged(_ sender: Any) {
        guard let category = self.category else {
            return
        }
        self.category?.allowed = self.switchSelection.isOn
        self.notificationByCategoryDelegate?.selectionChanged(category: category, selection: switchSelection.isOn)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        if let category = self.category {
            configureCategory(category)
        }
    }
    
}
