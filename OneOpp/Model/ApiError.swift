//
//  ApiError.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 25/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

class ApiError {
    let status: Int?
    let error: String?
    let message: String?
//    let errorCode: String?

    init(status: Int? = nil, error: String? = nil, message: String?) {
        self.status = status
        self.error = error
        self.message = message
//        self.data = data
    }

}

extension ApiError : Decodable {
    static func decode(_ json: JSON) -> Decoded<ApiError> {
        return curry(ApiError.init)
            <^> json <|? "status"
            <*> json <|? "error"
            <*> json <|? "message"
//            <*> json <|? "error_code"
    }
}
