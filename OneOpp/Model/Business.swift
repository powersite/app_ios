//
//  Business.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 26/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Business: Codable {
    
    let id: Int
    let name: String?
    let email: String?
    let thumbnail: String?
    let address: String?
    let number: String?
    let description: String?
    let latitud: String?
    let longitud: String?
    let mobile: String?
    let telephone: String?
    let stringCards: String?
    let status: Bool?
    let mpAccessToken: String?
    let mpPublicKey: String?
    
    var blackListed: Bool?
    
    var cards: [String]? {
        return self.stringCards?.split(separator: ",").map { String($0) }
    }
    var hasMPenabled: Bool {
        if let mpAccessToken = self.mpAccessToken,
           let mpPublicKey = self.mpPublicKey,
           !mpAccessToken.isEmpty,
           !mpPublicKey.isEmpty{
            return true
        } else {
            return false
        }
    }
}

extension Business : Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<Business> {
        return curry(self.init)
            <^> json <| "id"
            <*> json <|? "name"
            <*> json <|? "email"
            <*> json <|? "thumbnail"
            <*> json <|? "address"
            <*> json <|? "number"
            <*> json <|? "description"
            <*> json <|? "latitud"
            <*> json <|? "longitud"
            <*> json <|? "mobile"
            <*> json <|? "telephone"
            <*> json <|? "cards"
//            <*> .optional(json <| "cards" >>- toArray)
            <*> json <|? "status"
            <*> json <|? "mpAccessToken"
            <*> json <|? "mpPublicKey"
            <*> json <|? "blackListed"
    }
    
    private func toArray(cards: String) -> Decoded<[String]> {
        return .fromOptional(cards.split(separator: ",").map { String($0) })
    }
}
/*
 "business": {
     "id": 37,
     "name": "Runnies",
     "email": "r@gmail.com",
     "thumbnail": "5e7a45c10ccde_thumb.jpg",
     "address": "Diag 74",
     "number": "325",
     "description": "Picadas, sándwich, tortas.",
     "latitud": "-34.91520232959822",
     "longitud": "-57.95476164084471",
     "mobile": "123456",
     "telephone": "123456",
     "status": false
 },
 */
