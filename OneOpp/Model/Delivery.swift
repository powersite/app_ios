//
//  File.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 27/10/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Delivery: Codable {
    var id: Int?
    var benefitId: Int?
    var betweenStreet: String?
    var businessId: Int?
    var businessObservation: String?
    var deliveryStatusId: Int?
    var deliveryStatusName: String?
    var department: String?
    var deviceToken: String?
    var email: String?
    var extraData: String?
    var floor: Int?
    var name: String?
    var number: String?
    var phone: String?
    var street: String?
    var benefitDTO: AltBenefit?
}

extension Delivery : Decodable {
    static func decode(_ json: JSON) -> Decoded<Delivery> {
        let f = curry(Delivery.init)
            <^> json <| "id"
            <*> json <|? "benefitId"
            <*> json <|? "betweenStreet"
            <*> json <|? "businessId"
            <*> json <|? "businessObservation"
            <*> json <|? "deliveryStatusId"
            <*> json <|? "deliveryStatusName"
            <*> json <|? "department"
        return f
            <*> json <|? "deviceToken"
            <*> json <|? "email"
            <*> json <|? "extraData"
            <*> json <|? "floor"
            <*> json <|? "name"
            <*> json <|? "number"
            <*> json <|? "phone"
            <*> json <|? "street"
            <*> json <|? "benefitDTO"
    }
}
