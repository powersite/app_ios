//
//  User.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 26/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct User {
    let name: String?
    let email: String?
    let activated: Bool?
    var userImage: String?
}

extension User : Decodable {
    static func decode(_ json: JSON) -> Decoded<User> {
        return curry(User.init)
            <^> json <|? "name"
            <*> json <|? "email"
            <*> json <|? "activated"
            <*> json <|? "userImage"
    }
}
