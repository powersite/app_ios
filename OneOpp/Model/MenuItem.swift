//
//  MenuItem.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 07/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
struct MenuItem {
    let image: String
    let title: String
    let action: MenuAction
}
