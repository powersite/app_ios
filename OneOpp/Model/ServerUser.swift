//
//  ServerUser.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 15/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct ServerUser {
    
    let id: Int
    let name: String?
    let email: String?
    let activated: Bool?
    let createdBy: String?
    let createdDate: Date?
    let imageUrl: String?
    let langKey: String?
    let lastModifiedBy: String?
    let lastModifiedDate: Date?
}

extension ServerUser : Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<ServerUser> {
        return curry(self.init)
//        let f = curry(ServerUser.init)
            <^> json <| "id"
            <*> json <|? "name"
            <*> json <|? "email"
            <*> json <|? "activated"
//        return f
            <*> json <|? "createdBy"
            <*> .optional(json <| "createdDate" >>- toDate)
            <*> json <|? "imageUrl"
            <*> json <|? "langKey"
            <*> json <|? "lastModifiedBy"
            <*> .optional(json <| "lastModifiedDate" >>- toDate)
    }
}

private func toDate(dateInt: Int64) -> Decoded<Date> {
    return .fromOptional(Date(milliseconds: dateInt))
}

/*"user": {
  "activated": true,
  "createdBy": "string",
  "createdDate": "2020-06-14T18:58:32.110Z",
  "email": "string",
  "id": 0,
  "imageUrl": "string",
  "langKey": "string",
  "lastModifiedBy": "string",
  "lastModifiedDate": "2020-06-14T18:58:32.110Z",
  "name": "string"
}
*/
