//
//  Category.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 26/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Category: Codable {
    
    let id: Int
    let name: String?
    let position: Int?
    var allowed: Bool?
}

extension Category : Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<Category> {
        return curry(self.init)
            <^> json <| "id"
            <*> json <|? "name"
            <*> json <|? "position"
            <*> json <|? "allowed"
    }
    
}
/*
 "category": {
     "id": 1,
     "name": "Bebidas",
     "position": 1
 },
 */
