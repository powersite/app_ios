//
//  Benefit.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 26/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Benefit {
    let idBeneficio: Int
    let title: String?
    let description: String?
    let business: Business?
    let type: BenefitType?
    let status: String?
    let category: Category?
    let discount: BenefitDiscount?
    let image: String?
    let quantity: Int?
    let quantityTaken: Int?
    let price: Double?
    let quantityExchanged: Int?
    let reserved: Bool?
    let dates: Dates
    let delivery: Bool?
    let imageBenefitApp: String?
    var deliveryByCoupon: Delivery?
    var favorite: Bool?
}

extension Benefit : Decodable {
    static func decode(_ json: JSON) -> Decoded<Benefit> {
        let f = curry(Benefit.init)
            <^> json <| "id"
            <*> json <|? "title"
            <*> json <|? "description"
            <*> json <|? "business"
            <*> json <|? "benefitType"
            <*> json <|? "status"
        return f
            <*> json <|? "category"
            <*> json <|? "benefitDiscount"
            <*> json <|? "image"
            <*> json <|? "quantity"
            <*> json <|? "quantityTaken"
            <*> json <|? "price"
            <*> json <|? "quantityExchanged"
            <*> json <|? "reserved"
            <*> Dates.decode(json)
            <*> json <|? "delivery"
            <*> json <|? "imageBenefitApp"
            <*> json <|? "deliveryByCoupon"
            <*> json <|? "favourite"
    }
    
}

struct Dates: Codable {
    let days: [String]
    let startDate: Date?
    let endDate: Date?
    let creationDate: Date?
}
extension Dates : Decodable {
    static func decode(_ json: JSON) -> Decoded<Dates> {
        return curry(Dates.init)
            <^> json <|| "days"
            <*> .optional(json <| "startDate" >>- toDate)
            <*> .optional(json <| "endDate" >>- toDate)
            <*> .optional(json <| "creationDate" >>- toDate)
    }
}

private func toDate(dateInt: Int64) -> Decoded<Date> {
    return .fromOptional(Date(milliseconds: dateInt))
}

struct BenefitType: Encodable {
    let id: Int
    let name: String?
}

extension BenefitType : Decodable {
    static func decode(_ json: JSON) -> Decoded<BenefitType> {
        return curry(BenefitType.init)
            <^> json <| "id"
            <*> json <|? "name"
    }
}

struct AltBenefit: Codable {
    let idBeneficio: Int
    let title: String?
    let description: String?
    let business: Business?
    let category: Category?
//    let type: BenefitType?
//    let status: String?
//    let discount: BenefitDiscount?
//    let image: String?
//    let quantity: Int?
//    let quantityTaken: Int?
    let price: Double?
//    let quantityExchanged: Int?
//    let reserved: Bool?
//    let dates: Dates
//    let delivery: Bool?
//    let imageBenefitApp: String?
}

extension AltBenefit: Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<AltBenefit> {
        let f = curry(AltBenefit.init)
            <^> json <| "id"
            <*> json <|? "title"
            <*> json <|? "description"
            <*> json <|? "business"
//            <*> json <|? "category"
//            <*> json <|? "benefitType"
//            <*> json <|? "status"
        return f
              <*> json <|? "category"
//            <*> Category.decode(json)
//            <*> json <|? "benefitDiscount"
//            <*> json <|? "image"
//            <*> json <|? "quantity"
//            <*> json <|? "quantityTaken"
            <*> json <|? "price"
//            <*> json <|? "quantityExchanged"
//            <*> json <|? "reserved"
//            <*> Dates.decode(json)
//            <*> json <|? "delivery"
//            <*> json <|? "imageBenefitApp"
    }    
}
