//
//  UserSession.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 19/04/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
struct UserSession {
    var user: User
    var token: String
}
