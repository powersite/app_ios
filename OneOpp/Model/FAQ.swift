//
//  FAQ.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 09/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
struct FAQ: Codable {
    let main: String
    let detail: String
    
    init?(dictionary: [String: Any]) {
      guard let main = dictionary["main"] as? String else { return nil }
      guard let detail = dictionary["detail"] as? String else { return nil }
      
      self.main = main
      self.detail = detail
    }
}
