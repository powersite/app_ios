//
//  NotificationModel.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 04/09/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct NotificationModel: Encodable {
    let id: String
    let title: String?
    let discount: String?
    let business: String?
    let description: String?
    let color: String?
    let startdate: String?
    let enddate: String?
    let date: String?
}
extension NotificationModel : Decodable {
    static func decode(_ json: JSON) -> Decoded<NotificationModel> {
        let f = curry(NotificationModel.init)
            <^> json <| "id"
            <*> json <|? "title"
            <*> json <|? "discount"
            <*> json <|? "business"
            <*> json <|? "description"
        return f
            <*> json <|? "color"
            <*> json <|? "startdate"
            <*> json <|? "enddate"
            <*> json <|? "date"
//            <*> .optional(json <| "startdate" >>- toDate)
//            <*> .optional(json <| "enddate" >>- toDate)
//            <*> .optional(json <| "date" >>- toDate)
        
    }
    
}

//private func toDate(dateInt: Int64) -> Decoded<Date> {
//    return .fromOptional(Date(milliseconds: dateInt))
//}
