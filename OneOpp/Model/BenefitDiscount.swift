//
//  BenefitDiscount.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 26/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct BenefitDiscount {
    
    let id: Int
    let label: String?
    let position: Int?
    let color: String?
}

extension BenefitDiscount : Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<BenefitDiscount> {
        return curry(self.init)
            <^> json <| "id"
            <*> json <|? "label"
            <*> json <|? "position"
            <*> json <|? "color"
    }
    
}
