//
//  Coupon.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 14/06/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo
import Curry
import Runes

struct Coupon {
    let idCoupon: Int
    let benefit: Benefit?
    let code: Int?
    let creationDate: Date?
    let exchanged: Bool?
    let user: User?
}

extension Coupon : Decodable {
    static func decode(_ json: JSON) -> Decoded<Coupon> {
        return curry(Coupon.init)
            <^> json <| "id"
            <*> json <|? "benefit"
            <*> json <|? "code"
//            <*> json <|? "creationDate"
            <*> .optional(json <| "creationDate" >>- toDate)
            <*> json <|? "exchanged"
            <*> json <|? "user"
    }
    
}

private func toDate(dateInt: Int64) -> Decoded<Date> {
    return .fromOptional(Date(milliseconds: dateInt))
}

/*
 "code": 0,
 "creationDate": "2020-06-14T18:58:32.110Z",
 "exchanged": true,
 "id": 0,
 "user": {
   "activated": true,
   "createdBy": "string",
   "createdDate": "2020-06-14T18:58:32.110Z",
   "email": "string",
   "id": 0,
   "imageUrl": "string",
   "langKey": "string",
   "lastModifiedBy": "string",
   "lastModifiedDate": "2020-06-14T18:58:32.110Z",
   "name": "string"
 }
*/
