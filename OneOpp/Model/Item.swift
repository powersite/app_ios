//
//  Item.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 08/05/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import Foundation
import Argo

struct Item {
    let image: String?
    let description: String?
}
