//
//  AppDelegate.swift
//  OneOpp
//
//  Created by Vanina Geneiro on 18/03/2020.
//  Copyright © 2020 Vanina Geneiro. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase
import FirebaseMessaging
import Argo

let testnotification: [String: String] = [
    "id": "216",
    "title": "Descuento Unico",
    "discount": "60%",
    "business": "Bar John Doe",
    "description": "Sandwich vegano",
    "color": "#E63A25",
    "startdate": "2020-09-22 22:13:00",
    "enddate": "2020-09-27 22:13:00",
    "date": "2020-09-22 22:30:22"
]

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    var window: UIWindow?
//    var enteredByNotification: Bool = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        Skinning.styleMainTabBar()
        configureGoogleSignIn()
        
        // Override point for customization after application launch.
//        NavigationController.shared.showMainLoggedInNavigationScreen()
//        UserController.shared.logIn(email: "vaninagen@gmail.com", password: "oneopp", completionSuccess: {
//            print("success")
//            BenefitService.getBeneficios(page: 0, size: 10, orderedBy: .DATE, completionSucessfully: { (beneficios) in
//                print("cantidad de beneficios: \(beneficios?.count ?? 0)")
//            }) { (error) in
//                print(error)
//            }
            
//            UserController.shared.userProfile(completionSuccess: {
////                NavigationController.shared.showMainLoggedInNavigationScreen()
//            }) { (error) in
//                print(error?.errorDescription)
//            }
//        }) { (error) in
//            print(error.debugDescription)
//        }
        
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        
        let notificationOption = launchOptions?[.remoteNotification]
        // 1
        if let notification = notificationOption as? [String: AnyObject] {//,
//          let aps = notification["aps"] as? [String: AnyObject] {
          
          // 2
            let pn: NotificationModel? = decode(notification)
            if let notif = pn {
                NotificationsController.shared.addNotification(notif)
    
                return true
            }
        }
        
        // this won't execute if entering from notification
        if let onboardingShown = NavigationController.shared.onboardingShown, onboardingShown {
            UserController.shared.checkPreviousLogin(completionSuccess: {
                NavigationController.shared.showMainLoggedInNavigationScreen()
            }) { (_) in
                print("no previous login")
                NavigationController.shared.showLogInScreen()
            }
        } else {
            NavigationController.shared.showOnboarding()
        }
        return true
    }
    
    func configureGoogleSignIn() {
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        if let n: NotificationModel = decode(userInfo) {
            NotificationsController.shared.addNotification(n)

            self.openBenefitDetailWith(n.id)
        }

        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        let userInfo = notification.request.content.userInfo

        if let n: NotificationModel = decode(userInfo) {
            NotificationsController.shared.addNotification(n)

        }
        completionHandler([.alert, .badge, .sound])
    }
    
    func openBenefitDetailWith(_ benefitId: String) {
        BenefitService.getBenefitById(benefitId) { benefit in
            if let benefit = benefit {
                NavigationController.shared.showBenefitDetail(benefit)
            }
        } completionError: { error in
            print(error)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      Messaging.messaging().apnsToken = deviceToken
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken = fcmToken else {
            return
        }
        
      print("Firebase registration token: \(fcmToken)")

      let dataDict:[String: String] = ["token": fcmToken]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        UserDefaults.standard.set(fcmToken, forKey: "FCMToken")
        
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }

        
}
